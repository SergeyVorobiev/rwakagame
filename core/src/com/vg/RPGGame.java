package com.vg;

import com.badlogic.gdx.Game;

public class RPGGame extends Game {
    private MainScreen mainScreen;

    @Override
    public void create() {
        setScreen(mainScreen = new MainScreen(new GameInstruments()));
    }

    @Override
    public void dispose() {
        mainScreen.dispose();
    }
}
