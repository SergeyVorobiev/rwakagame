package com.vg.ui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.vg.GameInstruments;
import com.vg.L;
import com.vg.WorldMetric;
import com.vg.ai.assets.Assets;
import com.vg.objects.BuildingFactory;
import com.vg.storage.ObjectStorage;

public class MainMenu {
    private Table table;
    public final Stage stage;
    private ImageButton button;
    private ImageButton button2;
    private ImageButton button3;
    public MainMenu() {
        WorldMetric metric = GameInstruments.metric;
        this.stage = new Stage(new StretchViewport(metric.vWidthInMeters, metric.vHeightInMeters));
        table = new Table();
        table.setWidth(stage.getWidth());
        L.pr(table.getWidth());
        table.align(Align.left);
        table.setPosition(0, 2);
        button = getIB(Assets.buildIcon, true, l);
       // button.setZIndex(0);
        button2 = getIB(Assets.repair_menu, false, l);
       // button2.setZIndex(1);
        button3 = getIB(Assets.wall_menu, false, l);
       // button3.setZIndex(2);
        table.add(button).width(1).height(1);
        table.add(button2).width(1).height(1);
        //table.row().padBottom(0.1f);
        table.add(button3).width(1).height(1);
        //table.pack();
        stage.addActor(table);
    }
    private ImageButton getIB(Texture texture, boolean isVisible, InputListener l) {
        ImageButton ib = new ImageButton(new TextureRegionDrawable(new TextureRegion(texture)));
        ib.setVisible(isVisible);
        if(l != null)
            ib.addListener(l);
        return ib;
    }
    public void draw() {
       stage.draw();
    }

    private ClickListener l = new ClickListener() {
        public void clicked(InputEvent event, float x, float y) {
            Actor actor = event.getListenerActor();
            int index = actor.getZIndex();
            switch(index) {
                case 0:
                    button2.setVisible(!button2.isVisible());
                    button3.setVisible(!button3.isVisible());
                    break;
                case 1:
                    if(actor.isVisible() && ObjectStorage.objectByHand == null)
                        ObjectStorage.setObjectToHand(BuildingFactory.getFactory());
                    break;
                case 2:
                    if(actor.isVisible() && ObjectStorage.objectByHand == null)
                        ObjectStorage.setObjectToHand(BuildingFactory.getWall());
                    break;
            }
            /*
            if(ObjectStorage.objectByHand == null) {
                ObjectStorage.setObjectToHand(BuildingFactory.getFactory());
            }*/
        }
    };
}
