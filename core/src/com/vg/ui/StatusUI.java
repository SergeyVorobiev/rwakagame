package com.vg.ui;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.vg.L;
import com.vg.ai.assets.Assets;

public class StatusUI extends Window {

    private Image oneImage;
    private Image twoImage;
    //private TextButton button;
    private ImageButton button;
    private ImageButton button2;
    private Batch batch;
    public StatusUI(String title, Skin skin, Stage stage) {
        super("", skin);
        this.setStage(stage);
        stage.addActor(this);
        this.setWidth(4);
        this.setHeight(4);
        //stage.addActor(this);
        WidgetGroup wg = new WidgetGroup();
        wg.setWidth(2);
        wg.setHeight(2);
        batch = stage.getBatch();
        oneImage = new Image(new Texture("tiles/A.png"));
        twoImage = new Image(new Texture("tiles/B.png"));
        button = new ImageButton(new TextureRegionDrawable(new TextureRegion(Assets.buildIcon)));
        button2 = new ImageButton(new TextureRegionDrawable(new TextureRegion(Assets.BRICK)));
        button.addListener(l);
        button.setWidth(1);
        button.setHeight(1);
        //stage.addActor(button);
        /*
        button.addListener((e) -> {
            L.pr("hello");
            return true;
        });*/

        wg.addActor(button);
        //wg.addActor(button2);
        //stage.addActor(button);
       // wg.addActorAt(0, oneImage);
        //wg.addActorAt(1, twoImage);
        //wg.addActor(twoImage);

        this.add(wg).size(4, 4);
        this.row();
        this.pack();
    }
    private ClickListener l = new ClickListener() {
        public void clicked(InputEvent event, float x, float y) {
            L.pr("hello");
        }
    };

    public void draw(float alpha) {
        this.getStage().draw();
        batch.begin();
        //this.draw(batch, alpha);

        batch.end();
    }
}
