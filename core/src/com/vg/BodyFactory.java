package com.vg;

import com.badlogic.gdx.physics.box2d.*;

public class BodyFactory {
    public static Body createDynamicCircleBody(World world, float posX, float posY, float radius) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.position.set(posX, posY);
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        FixtureDef fixtureDef = new FixtureDef();
        CircleShape shape = new CircleShape();
        shape.setRadius(radius);
        fixtureDef.shape = shape;
        return world.createBody(bodyDef).createFixture(fixtureDef).getBody();
    }
}
