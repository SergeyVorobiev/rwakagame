package com.vg.storage;

import com.badlogic.gdx.utils.Array;
import com.vg.objects.BuildingObject;

public class ObjectStorage {
    public static BuildingObject objectByHand = null;
    public static final Array<BuildingObject> buildings = new Array<>();
    public static void setObjectToHand(BuildingObject object) {
        objectByHand = object;
        buildings.add(object);
    }
    public static void removeFromHandAndAdd() {
        objectByHand = null;
    }
    public static void removeFromHandAndCancel() {
        objectByHand = null;
        buildings.removeIndex(buildings.size - 1);
    }
}
