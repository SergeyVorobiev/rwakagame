package com.vg.storage;

import com.vg.ai.search.PathTracker;
import com.vg.ai.search.TileInfo;

import java.util.Arrays;
import java.util.TreeSet;

public class MaterialStorage {
    public static final TreeSet<TileInfo> tiles = new TreeSet<>((o1, o2) -> {
        int result = o1.x - o2.x;
        if(result == 0)
            return o1.y - o2.y;
        return result;
    });
    private static final int maxCapacity = 900;

    public static boolean containsMaterial(int materialType) {
        for(TileInfo tile: tiles) {
            if(tile.materialType == materialType)
                return true;
        }
        return false;
    }

    public static void addToStorage(TileInfo info, int count, int type) {
        info.materialCount += count;
        info.materialType = type;
        tiles.add(info);
    }
    public static void removeFromStorage(TileInfo info, int count) {
        info.materialCount -= count;
        if(info.materialCount == 0) {
            info.materialType = -1;
            tiles.remove(info);
        }
    }
    public static TileInfo[] getTilesWithMaterial(int materialType, TileInfo[] copy) {
        if (copy == null)
            copy = new TileInfo[tiles.size()];
        int i = 0;
        for(TileInfo tile : tiles) {
            if(tile.materialType == materialType) {
                copy[i++] = tile;
            }
        }
        Arrays.sort(copy, PathTracker::distanceComparator);
        return copy;
    }
    public static TileInfo getClosesTileWithMaterial(int materialType, TileInfo toTile) {
        int distance = Integer.MAX_VALUE;
        TileInfo result = null;
        for(TileInfo tile : tiles) {
            if(tile.materialType == materialType) {
                int curDistanceX = toTile.x - tile.x;
                int curDistanceY = toTile.y - tile.y;
                int curDistance = curDistanceX * curDistanceX + curDistanceY * curDistanceY;
                if (curDistance < distance) {
                    result = tile;
                }
            }
        }
        return result;
    }
}
