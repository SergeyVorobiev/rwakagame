package com.vg;

import com.badlogic.gdx.graphics.OrthographicCamera;

public interface OrthoCameraDriver {
    void handleDrive(float dt);
    OrthographicCamera getCamera();
}
