package com.vg;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.vg.ai.assets.Assets;
import com.vg.objects.BuildingObject;

public class BuildingDrawer implements DrawSelector<BuildingObject> {

    @Override
    public Drawer<BuildingObject> getCurrentDrawer(int state) {
        switch (state) {
            case BuildingObject.CANNOT_BUILD:
                return BuildingDrawer::cannotBuild;
            case BuildingObject.CANNOT_BE_PLACED:
                return BuildingDrawer::cannotBePlaced;
            case BuildingObject.CAN_BE_PLACED:
                return BuildingDrawer::canBePlaced;
            case BuildingObject.LAYOUT:
            case BuildingObject.LAYOUT_WITH_RESOURCES:
                return BuildingDrawer::layout;
            case BuildingObject.READY_TO_BUILD:
                return BuildingDrawer::readyToBuild;
            case BuildingObject.BUILDING:
                return BuildingDrawer::building;
            case BuildingObject.BUILT:
                return BuildingDrawer::built;
            default:
                return null;
        }
    }

    private static void cannotBuild(Batch batch, BuildingObject building) {
        return;
    }

    private static void cannotBePlaced(Batch batch, BuildingObject building) {
        batch.setColor(1f, 0.3f, 0.3f, 0.3f);
        batch.draw(Assets.sprites[building.type], building.startTile.x, building.startTile.y, building.tileWidth,
                building.tileHeight);
    }

    private static void canBePlaced(Batch batch, BuildingObject building) {
        batch.setColor(0.3f, 1, 0.3f, 0.3f);
        batch.draw(Assets.sprites[building.type], building.startTile.x, building.startTile.y, building.tileWidth, building.tileHeight);
    }

    private static void layout(Batch batch, BuildingObject building) {
        batch.setColor(1, 1, 1, 0.1f);
        batch.draw(Assets.sprites[building.type], building.startTile.x, building.startTile.y, building.tileWidth, building.tileHeight);
    }

    private static void readyToBuild(Batch batch, BuildingObject building) {
        batch.setColor(1, 1, 1, 0.3f);
        batch.draw(Assets.sprites[building.type], building.startTile.x, building.startTile.y, building.tileWidth, building.tileHeight);
    }

    private static void building(Batch batch, BuildingObject building) {
        batch.setColor(1, 1, 1, 0.3f);
        batch.draw(Assets.sprites[building.type], building.startTile.x, building.startTile.y, building.tileWidth, building.tileHeight);
        batch.setColor(1, 1, 1, 0.7f);
        batch.draw(Assets.sprites[building.type].getTexture(), building.startTile.x, building.startTile.y, building
                        .tileWidth, building.tileHeight * building.getProgress(), 0f, 0f, 1f, 1.f * building.getProgress());
    }

    private static void built(Batch batch, BuildingObject building) {
        batch.setColor(1, 1, 1, 1);
        batch.draw(Assets.sprites[building.type].getTexture(), building.startTile.x, building.startTile.y, building.tileWidth,
                building.tileHeight * building.getProgress(), 0f, 0f, 1f, 1.f * building.getProgress());
    }
}
