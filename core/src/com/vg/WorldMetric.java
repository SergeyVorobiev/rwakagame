package com.vg;

import com.badlogic.gdx.math.Rectangle;

public class WorldMetric {
    public final float tileSizeInPixels;
    public final int widthInTiles;
    public final int heightInTiles;
    public final float vWidthInMeters;
    public final float vHeightInMeters;
    public final float widthInMeters;
    public final float heightInMeters;
    public final float metersInPixel;
    public final float ratio;
    public WorldMetric(float tileSizeInPixels, int widthInTiles, int heightInTiles, float vWidthInTiles, float ratio) {
        this.tileSizeInPixels = tileSizeInPixels;
        this.widthInTiles = widthInTiles;
        this.heightInTiles = heightInTiles;
        this.metersInPixel = 1 / tileSizeInPixels;
        this.vWidthInMeters = vWidthInTiles;
        this.vHeightInMeters = vWidthInTiles / ratio;
        this.widthInMeters = widthInTiles;
        this.heightInMeters = heightInTiles;
        this.ratio = ratio;
    }

    public Rectangle convertToMeters(Rectangle rect) {
        return rect.set(rect.getX() * metersInPixel, rect.getY() * metersInPixel, rect.getWidth() * metersInPixel, rect.getHeight() * metersInPixel);
    }
}
