package com.vg.input;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.vg.GameInstruments;
import com.vg.L;
import com.vg.ai.graph.OrthoTileCameraDriver;
import com.vg.ai.search.TileInfo;
import com.vg.ai.tasks.BuildTask;
import com.vg.objects.BuildingObject;
import com.vg.storage.ObjectStorage;

public class GameInput implements InputProcessor {
    private OrthoTileCameraDriver camDriver;
    public GameInput() {
        camDriver = GameInstruments.camDriver;
    }
    @Override
    public boolean keyDown(int keycode) {
        L.pr("game");
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        int x = GameInstruments.camDriver.getXTileNumber(screenX);
        int y = GameInstruments.camDriver.getYTileNumber(screenY);
        BuildingObject object = ObjectStorage.objectByHand;
        if (button == Input.Buttons.LEFT) {

            if (object != null && object.state == BuildingObject.CAN_BE_PLACED) {
                object.place();
                ObjectStorage.removeFromHandAndAdd();
                GameInstruments.taskManager.accessibleTasks.add(new BuildTask());
            } else if(object == null && GameInstruments.isInBoundsX(x) && GameInstruments.isInBoundsY(y)){
                TileInfo info = GameInstruments.getTiles()[x][y];
                //if(info.getK() < 0)
                  //  GameInstruments.taskManager.accessibleTasks.add(new BrokeTask(info, 0));
            }
        } else if (button == Input.Buttons.RIGHT) {
            if(object != null)
                ObjectStorage.removeFromHandAndCancel();
            //Timer.start();
            //GameInstruments.pathFinder.searchTileWithMaterial(MaterialAssets.BRICK, x, y);
           // MaterialStorage
            //        .getClosesTileWithMaterial(MaterialAssets.BRICK, GameInstruments.searchEngine.getTileInfo(x, y));
            //Timer.stopAndPrint();
            /*
            TileInfo tile = GameInstruments.pathFinder.getTileInfo(x, y);
            DynamicObject obj = tile.placedObject;
            if(obj != null) {
                obj.getTask().releaseTask();
                obj.manualMode = !obj.manualMode;
            }
            */

        }
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
