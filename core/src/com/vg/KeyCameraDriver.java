package com.vg;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;
import com.vg.ai.graph.OrthoTileCameraDriver;

public class KeyCameraDriver implements OrthoTileCameraDriver {
    private float defDMove = 70;
    private float dMove = defDMove;
    private float zoom;
    private float dZoom = 5f;
    private float maxZoom = 20;
    private float minZoom = 0.7f;
    private OrthographicCamera camera;
    private float width;
    private float height;
    private int left;
    private int right;
    private int up;
    private int down;
    private Vector3 coords = new Vector3();

    public KeyCameraDriver(WorldMetric metric, float positionX, float positionY, float zoomFactor) {
        this.width = metric.vWidthInMeters;
        this.height = metric.vHeightInMeters;
        camera = new OrthographicCamera(width, height);
        camera.zoom = zoomFactor;
        camera.translate(positionX, positionY);
        zoom = zoomFactor;
        left = Input.Keys.LEFT;
        right = Input.Keys.RIGHT;
        up = Input.Keys.UP;
        down = Input.Keys.DOWN;
    }

    public KeyCameraDriver(WorldMetric metric, float positionX, float positionY) {
        this(metric, positionX, positionY, 1.0f);
    }

    public KeyCameraDriver(WorldMetric metric) {
        this(metric, 0, 0);
    }

    @Override
    public OrthographicCamera getCamera() {
        return camera;
    }

    public void setLeftKey(int key) {
        left = key;
    }

    public void setRightKey(int key) {
        right = key;
    }

    public void setUpKey(int key) {
        up = key;
    }

    public void setDownKey(int key) {
        down = key;
    }

    @Override
    public void handleDrive(float dt) {
        float move = dMove * dt;
        if(Gdx.input.isKeyPressed(left))
            camera.translate(-move, 0);
        if(Gdx.input.isKeyPressed(right))
            camera.translate(move, 0);
        if(Gdx.input.isKeyPressed(down))
            camera.translate(0, -move);
        if(Gdx.input.isKeyPressed(up))
            camera.translate(0, move);
        if(Gdx.input.isKeyPressed(Input.Keys.Z) && zoom < maxZoom) {
            camera.zoom += dZoom * dt;
        } else if(Gdx.input.isKeyPressed(Input.Keys.X) && zoom > minZoom) {
            camera.zoom -= dZoom * dt;
        }
        camera.update();
    }

    @Override
    public int getXTileNumber(int screenX) {
        float x = getXWorldCoord(screenX);
        if (x < 0)
            return -1;
        return (int) x;
    }

    @Override
    public int getYTileNumber(int screenY) {
        float y = getYWorldCoord(screenY);
        if (y < 0)
            return -1;
        return (int) y;
    }

    @Override
    public int getXScreenCoord(float xWorldCoord) {
        coords.x = xWorldCoord;
        return (int) camera.project(coords).x;
    }

    @Override
    public int getYScreenCoord(float yWorldCoord) {
        coords.y = yWorldCoord;
        return (int) camera.project(coords).y;
    }

    @Override
    public float getYWorldCoord(int screenY) {
        coords.y = screenY;
        return camera.unproject(coords).y;
    }

    @Override
    public float getXWorldCoord(int screenX) {
        coords.x = screenX;
        return camera.unproject(coords).x;
    }
}
