package com.vg;

import com.sun.istack.internal.NotNull;

public class ArrayHelper {

    public static void initDoubleIntArray(@NotNull int[][] array, int value) {
        int width = array.length;
        int height = array[0].length;
        for(int x = 0; x < width; x++) {
            for(int y = 0; y < height; y++) {
                array[x][y] = value;
            }
        }
    }

    public interface Supplier<T> {
        boolean supply(T t);
    }
    public static <T> void goThroughDoubleArray(@NotNull T[][] array, Supplier<T> sup) {
        int width = array.length;
        int height = array[0].length;
        boolean result = false;
        for(int x = 0; x < width; x++) {
            if (result) break;
            for(int y = 0; y < height; y++) {
                result = sup.supply(array[x][y]);
                if (result) break;
            }
        }
    }
}
