package com.vg.ai.search;

import com.badlogic.gdx.utils.Array;
import com.sun.istack.internal.NotNull;
import com.vg.GameInstruments;
import com.vg.L;

import java.util.ArrayList;
import java.util.PriorityQueue;

public class SearchEngine {

    public static final float sqr = (float) Math.sqrt(2);
    public static final float halfSqr = sqr / 2;
    private TileInfo[][] tiles;
    private PriorityQueue<TileInfo> queue = new PriorityQueue<>(500000);
    private Array<TileInfo> queueForArea = new Array<>(100000);
    private ArrayList<TileInfo> path = new ArrayList<>(5000);
    private int width;
    private int height;

    public SearchEngine(@NotNull TileInfo[][] tiles) {
        this.tiles = tiles;
        width = tiles.length;
        height = tiles[0].length;
    }

    public TileInfo[][] getTiles() {
        return tiles;
    }

    public TileInfo getTileInfo(int x, int y) {
        return tiles[x][y];
    }

    /**
     * Warning! Only for {@link PathTracker} usage.
     */
    public boolean buildPath(int startX, int startY, int endX, int endY, TileInfo[] pathCoords) {
        return buildPath(tiles[startX][startY], tiles[endX][endY], pathCoords);
    }

    public TileInfo searchTileWithMaterial(int material, int x, int y) {
        for (int i = 1; i < tiles.length; i++) {
            TileInfo tile = searchTileWithMaterialInRadius(material, x, y, i);
            if (tile != null)
                return tile;
        }
        return null;
    }

    public TileInfo searchTileWithMaterialInRadius(int material, int x, int y, int radius) {
        int width = tiles.length;
        int height = tiles[0].length;
        int j = 0;
        for (int cr = 1; cr <= radius; cr++) {
            int x1 = x - cr;
            int x2 = x + cr;
            int y1 = y - cr;
            int y2 = y + cr;
            if (y1 >= 0 && y1 < height) {
                for (int sx = x1; sx <= x2; sx++) {
                    if (sx >= 0 && sx < width)
                        if (tiles[sx][y1].materialType == material)
                            return tiles[sx][y1];
                }
            }
            if (y2 >= 0 && y2 < height) {
                for (int sx = x1; sx <= x2; sx++) {
                    if (sx >= 0 && sx < width)
                        if (tiles[sx][y2].materialType == material)
                            return tiles[sx][y2];
                }
            }
            if (x1 >= 0 && x1 < width) {
                for (int sy = y1 + 1; sy < y2; sy++) {
                    if (sy >= 0 && sy < height)
                        if (tiles[x1][sy].materialType == material)
                            return tiles[x1][sy];
                }
            }
            if (x2 >= 0 && x2 < width) {
                for (int sy = y1 + 1; sy < y2; sy++) {
                    if (sy >= 0 && sy < height)
                        if (tiles[x2][sy].materialType == material)
                            return tiles[x2][sy];
                }
            }
        }
        return null;
    }

    private boolean prepareState(TileInfo start, TileInfo end) {
        if (end.getK() < 0 || start == end)
            return false;
        if (start.getK() < 0)
            L.pr(start.x + " : " + start.y + " k < 0");
        count = 0;
        start.distanceFromStart = 0;
        start.distanceFromEnd = getDistanceFromEnd(start.x - end.x, start.y - end.y);
        start.sum = start.distanceFromEnd;
        queue.add(start);
        return true;
    }

    private void computePath(TileInfo end) {
        boolean result = false;
        while (!result) {
            TileInfo current = queue.poll();
            if (current == null)
                break;
            current.inQueue = false;
            TileInfo[] roundTiles = current.roundTiles;
            float currentK = current.getK();
            for (int i = 0; i < roundTiles.length; i++) {
                TileInfo roundTile = roundTiles[i];
                float roundK = roundTile == null ? -1 : roundTile.getK();
                if (roundK < 0 || roundTile.getPlacedObject() != null)
                    continue;
                if (i > 3 && !isDiagonal(i, current.x, current.y))
                    continue;
                if(roundTile == end) {
                    result = true;
                    roundTile.previous = current;
                    break;
                }
                float distanceFromStart = getDistanceFromCurrent(currentK, roundK, i) + current.distanceFromStart;
                if (distanceFromStart >= roundTile.distanceFromStart)
                    continue;
                if (roundTile.inQueue)
                    queue.remove(roundTile);
                roundTile.previous = current;
                roundTile.distanceFromStart = distanceFromStart;
                roundTile.distanceFromEnd = getDistanceFromEnd(roundTile.x - end.x, roundTile.y - end.y);
                roundTile.sum = roundTile.distanceFromEnd + roundTile.distanceFromStart;
                roundTile.inQueue = true;
                queue.add(roundTile);
            }
        }
    }

    private boolean isDiagonal(int direction, int x, int y) {
        switch (direction) {
            case TileInfoBuilder.RBOTTOM:
                return checkDiagonal(TileInfoBuilder.BOTTOM, TileInfoBuilder.RIGHT, x, y);
            case TileInfoBuilder.LBOTTOM:
                return checkDiagonal(TileInfoBuilder.BOTTOM, TileInfoBuilder.LEFT, x, y);
            case TileInfoBuilder.RTOP:
                return checkDiagonal(TileInfoBuilder.TOP, TileInfoBuilder.RIGHT, x, y);
            case TileInfoBuilder.LTOP:
                return checkDiagonal(TileInfoBuilder.TOP, TileInfoBuilder.LEFT, x, y);
        }
        return false;
    }

    /**
     * This method checks that if we want to go through diagonal, is there barrier which can intervene us.
     *
     * @param direction1 stub
     * @param direction2
     * @param x          stub
     * @param y          stub
     * @return stub
     */
    private boolean checkDiagonal(int direction1, int direction2, int x, int y) {
        TileInfo t = TileInfoBuilder.getTile(tiles, x, y, direction1);
        if (t != null && t.getK() < 0)
            return false;
        t = TileInfoBuilder.getTile(tiles, x, y, direction2);
        if (t != null && t.getK() < 0)
            return false;
        return true;
    }


    private static float getDistanceFromCurrent(float curK, float nextK, int direction) {
        float dirDist = (direction < 4 ? 0.5f : SearchEngine.halfSqr);
        return (curK + nextK) * dirDist;
    }

    private void collectCoords(TileInfo[] pathCoords, TileInfo end) {
        TileInfo info = end;
        while (info != null) {
            path.add(info);
            info = info.previous;
        }
        int j = 0;
        for (int i = path.size() - 1; i >= 0; i--) {
            pathCoords[j++] = path.get(i);
        }
        if (j < pathCoords.length) {
            pathCoords[j] = null;
        }
        path.clear();
    }

    public boolean buildPath(@NotNull TileInfo start, @NotNull TileInfo end, @NotNull TileInfo[] pathCoords) {
        if (!prepareState(start, end))
            return false;
        computePath(end);

        // Path is not found.
        if (end.previous == null) {
            clean();
            return false;
        }
        collectCoords(pathCoords, end);
        clean();
        return true;
    }

    private void clean() {
        queue.clear();
        for (int x = 0; x < width; x++) {
            TileInfo[] row = tiles[x];
            for (int y = 0; y < height; y++) {
                row[y].reset();
            }
        }
    }

    /**
     * Gets all closed areas.
     */
    public void initClosedAreas(AreaSearch as) {
        for (int x = 0; x < width; x++) {
            TileInfo[] wTiles = tiles[x];
            for (int y = 0; y < height; y++) {
                wTiles[y].areaNumber = -1;
            }
        }
        TileInfo[][] tiles = GameInstruments.getTiles();
        queueForArea.add(tiles[0][0]);
        int k = 0;
        for(int x = 0; x < tiles.length; x++) {
            TileInfo[] wTiles = tiles[x];
            for(int y = 0; y < wTiles.length; y++) {
                TileInfo t = wTiles[y];
                if(t.areaNumber != -1)
                    continue;
                if(t.getK() < 0)
                    continue;
                t.areaNumber = k;
                queueForArea.add(t);
                while (queueForArea.size != 0) {
                    TileInfo tile = queueForArea.removeIndex(queueForArea.size - 1);
                    TileInfo[] roundTiles = tile.roundTiles;
                    for (int i = 0; i < 4; i++) {
                        TileInfo at = roundTiles[i];
                        if (at != null && at.areaNumber == -1 && at.getK() > 0) {
                            at.areaNumber = k;
                            queueForArea.add(at);
                            //L.pr("not add");
                            //l.add(t);
                        }
                    }
                }
                k++;
            }
        }

        as.updateArea(tiles);
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public static int count = 0;

    private float getDistanceFromEnd(float dx, float dy) {
        return dx * dx + dy * dy;
    }
}
