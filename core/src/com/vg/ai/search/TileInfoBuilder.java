package com.vg.ai.search;

import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.sun.istack.internal.NotNull;
import com.vg.GameInstruments;

public class TileInfoBuilder {
    public static final int RIGHT = 0;
    public static final int LEFT = 1;
    public static final int TOP = 2;
    public static final int BOTTOM = 3;
    public static final int LTOP = 4;
    public static final int RTOP = 5;
    public static final int LBOTTOM = 6;
    public static final int RBOTTOM = 7;

    public static TileInfo[][] getNewTileMap(TiledMapTileLayer layer) {
        int width = layer.getWidth();
        int height = layer.getHeight();
        TileInfo[][] tiles = new TileInfo[width][height];
        for(int x = 0; x < width; x++) {
            for(int y = 0; y < height; y++) {
                TiledMapTileLayer.Cell cell = layer.getCell(x, y);
                tiles[x][y] = new TileInfo(x, y, cell == null ? 1 : -1);
            }
        }
        float distances;
        for(int x = 0; x < width; x++) {
            TileInfo[] rowTiles = tiles[x];
            for(int y = 0; y < height; y++) {
                TileInfo current = rowTiles[y];
                for(int i = 0; i < 8; i++) {
                    current.roundTiles[i] = getTile(tiles, x, y, i);
                }
            }
        }
        return tiles;
    }

    public static TileInfo[] getTilesAroundAsCube(@NotNull TileInfo[][] tiles, int x, int y, int radius, TileInfo[]
            infos) {
        int width = tiles.length;
        int height = tiles[0].length;
        int j = 0;
        for(int cr = 1; cr <= radius; cr++) {
            int x1 = x - cr;
            int x2 = x + cr;
            int y1 = y - cr;
            int y2 = y + cr;
            if (y1 >= 0 && y1 < height) {
                for (int sx = x1; sx <= x2; sx++) {
                    if (sx >= 0 && sx < width)
                        infos[j++] = tiles[sx][y1];
                }
            }
            if(y2 >= 0 && y2 < height) {
                for (int sx = x1; sx <= x2; sx++) {
                    if (sx >= 0 && sx < width)
                        infos[j++] = tiles[sx][y2];
                }
            }
            if(x1 >= 0 && x1 < width) {
                for (int sy = y1 + 1; sy < y2; sy++) {
                    if (sy >= 0 && sy < height)
                        infos[j++] = tiles[x1][sy];
                }
            }
            if(x2 >= 0 && x2 < width) {
                for (int sy = y1 + 1; sy < y2; sy++) {
                    if(sy >= 0 && sy < height)
                        infos[j++] = tiles[x2][sy];
                }
            }
        }
        if(j < infos.length)
            infos[j] = null;
        return infos;
    }

    // Warning! this method does not check correct positions of tiles.
    public static TileInfo[] getTilesArea(TileInfo[] tiles, TileInfo startTile, int width, int height) {
        TileInfo[][] area = GameInstruments.getTiles();
        int startX = startTile.x;
        int endX = startX + width;
        int startY = startTile.y;
        int endY = startY + height;
        //endX = endX < area.length ? endX : area.length - 1;
        //endY = endY < area[0].length ? endY : area[0].length - 1;
        int i = 0;
        for (int x = startX; x < endX; x++) {
            TileInfo[] areaX = area[x];
            for(int y = startY; y < endY; y++) {
                tiles[i++] = areaX[y];
            }
        }
        return tiles;
    }
    // Start corner is left bottom corner.
    public static TileInfo[] getPerimeter(@NotNull TileInfo[] result, int x, int
            y, int width, int height) {
        TileInfo[][] tiles = GameInstruments.getTiles();
        int endMapX = tiles.length - 1;
        int endMapY = tiles[0].length - 1;
        int endX = x + width;
        int endY = y + height;
        //*** Gets above and below rows.
        //. .
        //***
        // Take bottom horizontal line of perimeter.
        int k = 0;

        if (GameInstruments.isInBoundsY(y)) {
            for (int i = x; i < endX; i++) {
                if (i < 0)
                    continue;
                if (i > endMapX)
                    break;
                result[k++] = tiles[i][y];
            }
        }

        // Take top horizontal line of perimeter.
        if (GameInstruments.isInBoundsY(endY)) {
            for (int i = x; i < endX; i++) {
                if (i < 0)
                    continue;
                if (i > endMapX)
                    break;
                result[k++] = tiles[i][endY - 1];
            }
        }

        // Take left vertical line of perimeter without first tile.
        if (GameInstruments.isInBoundsX(x)) {
            for (int i = y + 1; i < endY - 1; i++) {
                if (i < 0)
                    continue;
                if (i > endMapY)
                    break;
                result[k++] = tiles[x][i];
            }
        }

        // Take right vertical line of perimeter without first tile.
        if (GameInstruments.isInBoundsX(endX)) {
            for (int i = y + 1; i < endY - 1; i++) {
                if (i < 0)
                    continue;
                if (i > endMapY)
                    break;
                result[k++] = tiles[endX - 1][i];
            }
        }
        if(k < result.length)
            result[k] = null;
        return result;
    }

    public static TileInfo getTile(@NotNull TileInfo[][] tiles, int x, int y, int direction) {
        int width = tiles.length;
        int height = tiles[0].length;
        switch(direction) {
            case RIGHT:
                return x + 1 < width ? tiles[x + 1][y] : null;
            case LEFT:
                return x - 1 >= 0 ? tiles[x - 1][y] : null;
            case BOTTOM:
                return y - 1 >= 0 ? tiles[x][y - 1] : null;
            case TOP:
                return y + 1 < height ? tiles[x][y + 1] : null;
            case LTOP:
                return  x - 1 >= 0 && y + 1 < height ? tiles[x - 1][y + 1] : null;
            case RBOTTOM:
                return  x + 1 < width && y - 1 >= 0 ? tiles[x + 1][y - 1] : null;
            case RTOP:
                return x + 1 < width && y + 1 < height ? tiles[x + 1][y + 1] : null;
            case LBOTTOM:
                return x - 1 >= 0 && y - 1 >= 0 ? tiles[x - 1][y - 1] : null;
            default:
                return null;
        }
    }
}
