package com.vg.ai.search;

import com.vg.GameInstruments;
import com.vg.WorldMetric;

public class AreaSearch {
    public final Object block = new Object();
    public final int[][] area;
    public AreaSearch() {
        WorldMetric metric = GameInstruments.metric;
        area = new int[metric.widthInTiles][metric.heightInTiles];

    }

    public void updateArea(TileInfo[][] infos) {
        synchronized (block) {
            for (int x = 0; x < area.length; x++) {
                int[] wArea = area[x];
                TileInfo[] info = infos[x];
                for (int y = 0; y < area[x].length; y++) {
                    wArea[y] = info[y].areaNumber;
                }
            }
        }
    }
}
