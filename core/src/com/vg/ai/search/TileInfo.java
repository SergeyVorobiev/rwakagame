package com.vg.ai.search;

import com.vg.objects.DynamicObject;
import com.vg.objects.GameObject;

public class TileInfo implements Comparable<TileInfo> {
    public final int x;
    public final int y;

    // Uses to set distance from this to object to sort some tiles.
    public float tempDistanceToObject;
    private float k;

    // Warning! Only for SearchEngine use.
    public float distanceFromEnd = Float.MAX_VALUE;

    // Warning! Only for SearchEngine use.
    public float distanceFromStart = Float.MAX_VALUE;

    // Warning! Only for SearchEngine use.
    public float sum;

    // Warning! Only for SearchEngine use.
    public TileInfo previous;

    // Warning! Only for SearchEngine use.
    public boolean inQueue;

    // Warning! Only for SearchEngine use.
    public int areaNumber = -1;

    // Some building on the tile.
    public GameObject staticObject;
    private DynamicObject placedObject;
    private Object blocked = new Object();
    public int materialCount = 0;
    public int materialType = -1;
    public final TileInfo[] roundTiles = new TileInfo[8];
    public TileInfo(int x, int y, float k) {
        this.x = x;
        this.y = y;
        this.k = k;
    }

    public void setPlacedObject(DynamicObject placedObject) {
        synchronized (blocked) {
            this.placedObject = placedObject;
        }
    }

    public void setK(float k) {
        synchronized (blocked) {
            this.k = k;
        }
    }

    public float getK() {
        float innerK;
        synchronized (blocked) {
            innerK = k;
        }
        return innerK;
    }

    public boolean isEmpty() {
        boolean result;
        synchronized (blocked) {
            result = placedObject == null && k > 0;
        }
        return result;
    }
    public DynamicObject getPlacedObject() {
        DynamicObject innerObject;
        synchronized (blocked) {
            innerObject = placedObject;
        }
        return innerObject;
    }

    // Warning! Only for SearchEngine use.
    public void reset() {
        distanceFromEnd = Float.MAX_VALUE;
        distanceFromStart = Float.MAX_VALUE;
        sum = Float.MAX_VALUE;
        inQueue = false;
        previous = null;
    }

    public String toString() {
        return String.valueOf("x: " + x + "; y: " + y);
    }

    @Override
    public int compareTo(TileInfo o) {
        if (this == o)
            return 0;
        float result = this.sum - o.sum;

        //float result = this.distanceFromStart - o.distanceFromStart;
        if (result > 0) {
            return 1;
        } else if (result < 0) {
            return -1;
        } else {
            result = this.distanceFromStart - o.distanceFromStart;
            if (result > 0) {
                return 1;
            } else if (result < 0) {
                return -1;
            } else {
                return 0;
            }
        }
    }
}
