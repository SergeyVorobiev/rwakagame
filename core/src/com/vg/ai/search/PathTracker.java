package com.vg.ai.search;

import com.sun.istack.internal.NotNull;
import com.vg.GameInstruments;
import com.vg.L;
import com.vg.objects.DynamicObject;

import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class PathTracker {
    public final TileInfo[] pathCoords;
    private int curPathIndex;
    private TileInfo targetTile;
    private float posX;
    private float posY;
    private int directionX;
    private int directionY;
    private TileInfo endTile;
    private DynamicObject parent;
    public Object blocked = new Object();
    private Future<Boolean> future;
    public static final int TOP = 0;
    public static final int BOTTOM = 1;
    public static final int LEFT = 2;
    public static final int RIGHT = 3;
    public static final int TOP_RIGHT = 4;
    public static final int TOP_LEFT = 5;
    public static final int BOTTOM_RIGHT = 6;
    public static final int BOTTOM_LEFT = 7;
    public static final int NONE = -1;
    private int direction = NONE;
    public static final int IN_POSITION = 0;
    public static final int CANNOT_REACH = 1;
    public static final int MOVING = 2;
    public static final int SEARCHING_PATH = 3;
    private int status = IN_POSITION;
    public PathTracker(DynamicObject parent) {
        pathCoords = new TileInfo[GameInstruments.WORLD_WIDTH_IN_TILES * GameInstruments.WORLD_HEIGHT_IN_TILES];
        targetTile = parent.startTile;
        endTile = targetTile;
        this.posX = targetTile.x;
        this.posY = targetTile.y;
        endTile.setPlacedObject(parent);
        this.parent = parent;
    }

    public int getTargetTileX() {
        return targetTile.x;
    }

    public int getTargetTileY() {
        return targetTile.y;
    }

    public float getPosX() {
        return posX;
    }

    public float getPosY() {
        return posY;
    }

    public TileInfo[] getTilesSortedByDistance(@NotNull TileInfo[] infos, TileInfo[] copy) {
        if (copy == null)
            copy = new TileInfo[infos.length];
        for (int i = 0; i < infos.length; i++) {
            TileInfo info = infos[i];
            if(info == null)
                continue;
            if(!info.isEmpty() && info.getPlacedObject() != this.parent) {
                info.tempDistanceToObject = Integer.MAX_VALUE;
                copy[i] = info;
                continue;
            }
            float distanceX = info.x - targetTile.x;
            float distanceY = info.y - targetTile.y;
            info.tempDistanceToObject = distanceX  * distanceX + distanceY * distanceY;
            copy[i] = info;
        }
        Arrays.sort(copy, PathTracker::distanceComparator);
        return copy;
    }

    public static int distanceComparator(TileInfo one, TileInfo two) {
        if(one == null && two == null)
            return 0;
        if(one == null)
            return 1;
        if (two == null)
            return -1;
        return one.tempDistanceToObject - two.tempDistanceToObject > 0 ? 1 : -1;
    }

    public TileInfo getClosesEmptyTile(@NotNull TileInfo[] infos) {
        TileInfo closesInfo = null;
        float distance = Float.MAX_VALUE;
        for(int i = 0; i < infos.length; i++) {
            TileInfo info = infos[i];
            if(info == null || !info.isEmpty())
                continue;
            float distanceX = info.x - targetTile.x;
            float distanceY = info.y - targetTile.y;
            float result = distanceX  * distanceX + distanceY * distanceY;
            if(result < distance) {
                distance = result;
                closesInfo = info;
            }
        }
        return closesInfo;
    }

    public int getCurrentStatus() {
        int innerStatus;
        synchronized (blocked) {
            innerStatus = status;
        }
        return innerStatus;
    }

    public int getDirection() {
        int innerDirection;
        synchronized (blocked) {
            innerDirection = direction;
        }
        return innerDirection;
    }

    // This method for all objects should be invoked in one thread only if we do not move.
    private boolean search() {
        SearchEngine pf = GameInstruments.searchEngine;
        //Timer.start();
        TileInfo curTargetTile;
        TileInfo curEndTile;
        synchronized (blocked) {
            curTargetTile = targetTile;
            curEndTile = endTile;
        }
        boolean result = pf.buildPath(curTargetTile, curEndTile, pathCoords);
        //Timer.stopAndPrint();
        if (result) {

            // We release current place because start to move.
            targetTile.setPlacedObject(null);

            // We hold 'end' place that anybody other could not go to it.
            endTile.setPlacedObject(parent);
            curPathIndex = 1;
            targetTile = pathCoords[curPathIndex];
            setStatus(MOVING);
            initDirection();
            return true;
        } else {
            if (endTile.getPlacedObject() == parent)
                endTile.setPlacedObject(null);
            targetTile.setPlacedObject(parent);
            setStatus(CANNOT_REACH);
            direction = NONE;
            return false;
        }
    }

    private void setStatus(int status) {
        synchronized (blocked) {
            this.status = status;
        }
    }
    public int goToPlace(TileInfo end) {
        if(end == null) {
            setStatus(CANNOT_REACH);
            return CANNOT_REACH;
        }
        if (endTile == end) {
            status = getCurrentStatus();
            if(status == CANNOT_REACH) {
                endTile = null;
            }
            return status;
        }
        endTile = end;
        return findPath();
    }

    private int findPath() {
        int tempStatus = getCurrentStatus();
        if(tempStatus == SEARCHING_PATH) {
            try {
                // If it searching path we need to wait while searching end and reset all found path.
                future.get();
            }catch(Exception e) {

            }
            future = GameInstruments.searchExecutor.submit(this::search);
        } else if (tempStatus == CANNOT_REACH || tempStatus == IN_POSITION){
            status = SEARCHING_PATH;
            future = GameInstruments.searchExecutor.submit(this::search);
        } else {
            curPathIndex = pathCoords.length;
        }
        return getCurrentStatus();
    }
    public void move(float speed, float dt) {
        synchronized(blocked) {
            if (status != MOVING)
                return;
            moveToTarget(speed, dt);

            // if after moving it has over jumped target.
            if (isOverJump())
                computeNextPoint();
        }
    }

    private void moveToTarget(float speed, float dt) {
        if(directionX != 0 && directionY != 0) {
            posX += speed * dt * SearchEngine.halfSqr * directionX;
            posY += speed * dt * SearchEngine.halfSqr * directionY;
        } else if(directionX != 0) {
            posX += speed * dt * directionX;
        } else if(directionY != 0) {
            posY += speed * dt * directionY;
        } else {
            L.re("should move but cant");
        }
    }
    private void initDirection() {
        float distanceX = targetTile.x - posX;
        float distanceY = targetTile.y - posY;
        int innerDirection;
        if(distanceX > 0 && distanceY == 0) {
            innerDirection = RIGHT;
            directionX = 1;
            directionY = 0;
        } else if(distanceX > 0 && distanceY > 0) {
            innerDirection = TOP_RIGHT;
            directionX = 1;
            directionY = 1;
        } else if(distanceX > 0 && distanceY < 0) {
            innerDirection = BOTTOM_RIGHT;
            directionX = 1;
            directionY = -1;
        } else if(distanceX < 0 && distanceY == 0) {
            innerDirection = LEFT;
            directionX = -1;
            directionY = 0;
        } else if(distanceX < 0 && distanceY > 0) {
            innerDirection = TOP_LEFT;
            directionX = -1;
            directionY = 1;
        } else if(distanceX < 0 && distanceY < 0) {
            innerDirection = BOTTOM_LEFT;
            directionX = -1;
            directionY = -1;
        } else if(distanceX == 0 && distanceY > 0) {
            innerDirection = TOP;
            directionX = 0;
            directionY = 1;
        } else if(distanceX == 0 && distanceY < 0){
            innerDirection = BOTTOM;
            directionX = 0;
            directionY = -1;
        } else {
            innerDirection = NONE;
            directionX = 0;
            directionY = 0;
        }
        synchronized (blocked) {
            direction = innerDirection;
        }
    }

    private boolean isOverJump() {
        float distanceToTargetX = targetTile.x - posX;
        float distanceToTargetY = targetTile.y - posY;
        return ((distanceToTargetX <= 0 && directionX >= 0) || (distanceToTargetX >= 0 && directionX <= 0)) &&
                ((distanceToTargetY <= 0 && directionY >= 0) || (distanceToTargetY >= 0 && directionY <= 0));
    }

    public void stopNow() {
        synchronized (blocked) {
            if (status == MOVING) {
                posX = targetTile.x;
                posY = targetTile.y;
                endTile = targetTile;
                status = CANNOT_REACH;
                direction = NONE;
            } else if(status == SEARCHING_PATH) {
                try {
                    future.get();
                    posX = targetTile.x;
                    posY = targetTile.y;
                    endTile = targetTile;
                    status = CANNOT_REACH;
                    direction = NONE;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void stop() {
        synchronized (blocked) {
            if (status == MOVING) {
                endTile = targetTile;
            } else if(status == SEARCHING_PATH) {
                try {
                    future.get();
                    posX = targetTile.x;
                    posY = targetTile.y;
                    endTile = targetTile;
                    status = CANNOT_REACH;
                    direction = NONE;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void computeNextPoint() {
        curPathIndex++;
        posX = targetTile.x;
        posY = targetTile.y;

        // We finished.
        if (targetTile == endTile) {
            status = IN_POSITION;
            direction = NONE;

        // We is not on the place and we need calculate path.
        } else if(curPathIndex >= (pathCoords.length) || pathCoords[curPathIndex] == null) {
            status = CANNOT_REACH;
            findPath();

        // We get a barrier and we need calculate path.
        } else if(curPathIndex < pathCoords.length && pathCoords[curPathIndex].getK() < 0) {
            status = CANNOT_REACH;
            findPath();
        } else if(pathCoords[curPathIndex + 1] == null && pathCoords[curPathIndex].getPlacedObject() !=
                this.parent) {
            throw new RuntimeException("Awful");
        } else {
            targetTile = pathCoords[curPathIndex];
            initDirection();
        }
    }
}
