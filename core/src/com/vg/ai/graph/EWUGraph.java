package com.vg.ai.graph;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

public class EWUGraph implements IUGraph {
	private TreeMap<Integer, TreeSet<Edge>> map = new TreeMap<Integer, TreeSet<Edge>>();
	private int sizeE = 0;
	
	public EWUGraph() {
		
	}
	public EWUGraph(String pathToFile) {
		Scanner s = null;
		try {
			int[] v = new int[3];
			s = new Scanner(new BufferedInputStream(new FileInputStream(pathToFile)));
			while(s.hasNext()) {
				StringTokenizer st = new StringTokenizer(s.nextLine(), ";");
				if(st.countTokens() != 3) continue;
				int count = 0;
				while(st.hasMoreTokens()) {
					try {
						Integer i = Integer.valueOf(st.nextToken());
						v[count++] = i;
					} catch(NumberFormatException nfe) {
						break;
					}
				}
				addEdge(v[0], v[1], v[2]);
			}
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public boolean addEdge(Edge e) {
		int v1 = e.getV1();
		int v2 = e.getV2();
		if(v1 == v2) return false;
		TreeSet<Edge> tree1 = map.get(v1);
		TreeSet<Edge> tree2 = map.get(v2);
		if(tree1 == null) {
			tree1 = newTreeSet();
			map.put(v1, tree1);
		}
		if(tree2 == null) {
			tree2 = newTreeSet();
			map.put(v2, tree2);
		}
		tree1.add(e);
		boolean result = tree2.add(e);
		if(result == true) sizeE++;
		return result;
	}
	private TreeSet<Edge> newTreeSet() {
		return new TreeSet<Edge>(new Comparator<Edge>() {
			@Override
			public int compare(Edge e1, Edge e2) {
				int result = e1.compareTo(e2);
				if(result != 0) return result;
				if(e1.equals(e2)) return 0;
				if(e1.getV1() < e2.getV1()) return -1;
				if(e1.getV1() > e2.getV1()) return 1;
				if(e1.getV2() < e2.getV2()) return -1;
				return 1;
			}
		});
	}
	public boolean addEdge(int v1, int v2) {
		return addEdge(v1, v2, 0);
	}
	public boolean addEdge(int v1, int v2, int weight) {
		if(v1 == v2) return false;
		return addEdge(new Edge(v1, v2, weight));
	}
	public Iterable<Edge> getAdjacentE(int v) {
		Iterable<Edge> iter = map.get(v);
		if(iter == null) return new TreeSet<Edge>();
		return iter;
	}
	@Override
	public int getSizeVertices() {
		return map.lastKey() + 1;
	}
	public Iterable<Edge> getEdges() {
		TreeSet<Edge> set = newTreeSet();
		for(int v = 0; v < getSizeVertices(); v++) {
			for(Edge e : getAdjacentE(v)) {
				if(e.getOther(v) > v) set.add(e);
			}
		}
		return set;
	}
	@Override
	public int getSizeEdges() {
		return sizeE;
	}
	@Override
	public Iterable<Integer> getAdjacent(int v) {
		LinkedList<Integer> list = new LinkedList<Integer>();
		for(Edge e : getAdjacentE(v)) {
			list.add(e.getOther(v));
		}
		return list;
	}
}
