package com.vg.ai.graph;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.TreeSet;

public class EWDiGraph implements IDiGraph {
	private int sizeE = 0;
	private TreeMap<Integer, TreeSet<DiEdge>> map = new TreeMap<Integer, TreeSet<DiEdge>>();
	
	public EWDiGraph() {
		
	}
	
	public EWDiGraph(String pathToFile) {
		Scanner s = null;
		try {
			int[] v = new int[3];
			s = new Scanner(new BufferedInputStream(new FileInputStream(pathToFile)));
			while(s.hasNext()) {
				StringTokenizer st = new StringTokenizer(s.nextLine(), ";");
				if(st.countTokens() != 3) continue;
				int count = 0;
				while(st.hasMoreTokens()) {
					try {
						Integer i = Integer.valueOf(st.nextToken());
						v[count++] = i;
					} catch(NumberFormatException nfe) {
						break;
					}
				}
				addEdge(v[0], v[1], v[2]);
			}
			s.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public boolean addEdge(int from, int to, double weight) {
		return addEdge(new DiEdge(from, to, weight));
	}
	public boolean addEdge(int from, int to) {
		return addEdge(new DiEdge(from, to, 0));
	}
	public boolean addEdge(DiEdge e) {
		int from = e.getFrom();
		int to = e.getTo();
		if(from == to) return false;
		TreeSet<DiEdge> tree = map.get(from);
		if(tree == null) {
			tree = newTreeSet();
			map.put(from, tree);
		}
		if(map.get(to) == null) map.put(to, newTreeSet());
		boolean result = tree.add(e);
		if(result) sizeE++;
		return result;
	}
	@Override
	public int getSizeEdges() {
		return sizeE;
	}
	@Override
	public int getSizeVertices() {
		return map.lastKey() + 1;
	}
	@Override
	public Collection<Integer> getAdjacent(int v) {
		LinkedList<Integer> list = new LinkedList<Integer>();
		for(DiEdge e : getAdjacentE(v)) {
			list.add(e.getOther(v));
		}
		return list;
	}
	public Collection<DiEdge> getAdjacentE(int v) {
		TreeSet<DiEdge> tree = map.get(v);
		if(tree == null) tree = new TreeSet<DiEdge>();
		return tree;
	}
	@Override
	public EWDiGraph getReverse() {
		EWDiGraph g = new EWDiGraph();
		for(TreeSet<DiEdge> t : map.values()) {
			for(DiEdge e : t) {
				g.addEdge(e.getTo(), e.getFrom(), e.getWeight());
			}
		}
		return g;
	}
	public static TreeSet<DiEdge> newTreeSet() {
		return new TreeSet<DiEdge>(new Comparator<DiEdge>() {
			@Override
			public int compare(DiEdge e1, DiEdge e2) {
				int result = e1.compareTo(e2);
				if(result != 0) return result;
				if(e1.equals(e2)) return 0;
				if(e1.getFrom() < e2.getFrom()) return -1;
				if(e1.getFrom() > e2.getFrom()) return 1;
				if(e1.getTo() < e2.getTo()) return -1;
				return -1;
			}
		});
	}
	public Collection<DiEdge> getEdges() {
		TreeSet<DiEdge> tree = newTreeSet();
		for(TreeSet<DiEdge> t : map.values()) {
			tree.addAll(t);
		}
		return tree;
	}
}
