package com.vg.ai.graph;

import com.vg.OrthoCameraDriver;

public interface OrthoTileCameraDriver extends OrthoCameraDriver {
    int getXTileNumber(int screenX);
    int getYTileNumber(int screenY);
    float getXWorldCoord(int screenX);
    float getYWorldCoord(int screenY);
    int getXScreenCoord(float xWorldCoord);
    int getYScreenCoord(float yWorldCoord);
}
