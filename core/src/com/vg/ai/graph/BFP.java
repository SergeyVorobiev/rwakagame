package com.vg.ai.graph;

import java.util.LinkedList;
import java.util.Queue;

public class BFP {
	private boolean[] marked;
	private int[] edgeTo;
	private int count;
	private int start;
	public BFP(IUGraph g, int v) {
		start = v;
		marked = new boolean[g.getSizeVertices()];
		edgeTo = new int[g.getSizeVertices()];
		bfs(g, v);
	}
	private void bfs(IUGraph g, int v) {
		Queue<Integer> q = new LinkedList<Integer>();
		q.add(v);
		marked[v] = true;
		while(!q.isEmpty()) {
			v = q.remove();
			for(int w : g.getAdjacent(v)) {
				if(!marked[w]) {
					q.add(w);
					marked[w] = true;
					edgeTo[w] = v;
				}
			}
		}
	}
	public boolean isMarked(int w) {return marked[w];}
	public int getCount(){return count;}
	public Iterable<Integer> getPathTo(int v) {
		if(!isMarked(v)) return null;
		MyStack<Integer> s = new MyStack<Integer>();
		for(int i = v; i != start; i = edgeTo[i]) {
			s.add(i);
		}
		s.add(start);
		return s;
	}
}
