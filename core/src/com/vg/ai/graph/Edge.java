package com.vg.ai.graph;

public class Edge implements Comparable<Edge> {
	private int v1;
	private int v2;
	private double weight;
	public Edge(int v1, int v2, double weight) {
		this.v1 = v1;
		this.v2 = v2;
		this.weight = weight;
	}
	public int getV1() {
		return v1;
	}
	public int getV2() {
		return v2;
	}
	public double getWeight() {
		return weight;
	}
	@Override
	public String toString() {
		return v1 + " - " + v2 + " (" + weight + ")"; 
		
	}
	public int getOther(int v) {
		if(v == v1) return v2;
		if(v == v2) return v1;
		throw new RuntimeException("Incorrect v: " + v);
	}
	@Override
	public int compareTo(Edge e) {
		if(this.getWeight() > e.getWeight()) return 1;
		if(this.getWeight() < e.getWeight()) return -1;
		return 0;
	}
	@Override
	public boolean equals(Object o) {
		Edge e = (Edge)o;
		if((this.getV1() == e.getV1() && this.getV2() == e.getV2()) ||
				this.getV2() == e.getV1() && this.getV1() == e.getV2()) return true;
		return false;
	}
}
