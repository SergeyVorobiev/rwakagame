package com.vg.ai.graph;

public interface IUGraph {
	public int getSizeVertices();
	public int getSizeEdges();
	public Iterable<Integer> getAdjacent(int v);
}
