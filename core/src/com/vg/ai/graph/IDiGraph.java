package com.vg.ai.graph;

public interface IDiGraph extends IUGraph {
	public IDiGraph getReverse();
}
