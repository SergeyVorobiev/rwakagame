package com.vg.ai.graph;

public class PQueueC <T> {
	private int capacity;
	private int size = 0;
	private Comparable[] array;
	public int curIndex1 = 0;
	public int curIndex2 = 0;
	public int addedIndex = 0;
	public PQueueC() {
		this(1000);
	}
	public PQueueC(int capacity) {
		this.capacity = capacity;
		array = new Comparable[capacity];
	}
	public void add(Comparable element) {
		if(size == capacity) {
			capacity = capacity * 2;
			Comparable[] temp = new Comparable[capacity];
			for(int i = 0; i < size; i++) {
				temp[i] = array[i];
			}
			array = temp;
		}
		array[size++] = element;
		up();
	}
	public Comparable getValue(int index) {
		if(index >= size) return 0;
		return array[index];
	}
	
	public void up() {
		int k = size;
		int half;
		while(k > 1 && less(array[(half = (k >> 1)) - 1], array[k - 1])) {
			swap(half - 1, k - 1);
			k = half;
		}
	}
	private void swap(int index1, int index2) {
		Comparable temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
	}
	private boolean less(Comparable value1, Comparable value2) {
		return value1.compareTo(value2) > 0;
	}
	public int getSize() {
		return size;
	}
	private void down() {
		int k = 0;
		int j;
		int twok;
		while((twok = (k << 1)) < (size - 1)) {
			j = twok + 1;
			if(j < (size - 1) && less(array[j], array[j+1]))
			    j++;
			if(!less(array[k], array[j]))
			    break;
			swap(k, j);
			k = j;
		}
	}
	public T poll() {
		if(size > 0) {
			Comparable result = array[0];
			array[0] = array[--size];
			down();
			return (T) result;
		}
		return null;
	}

	public void clear() {
	    for(int i = 0; i < array.length; i++)
	        array[i] = null;
    }
}
