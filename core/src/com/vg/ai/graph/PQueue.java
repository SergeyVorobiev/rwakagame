package com.vg.ai.graph;

public class PQueue {
	private int capacity;
	private int size = 0;
	private int[] array;
	public int curIndex1 = 0;
	public int curIndex2 = 0;
	public int addedIndex = 0;
	public PQueue() {
		this(1000);
	}
	public PQueue(int capacity) {
		this.capacity = capacity;
		array = new int[capacity];
	}
	public void add(int element) {
		if(size == capacity) {
			capacity = capacity * 2;
			int[] temp = new int[capacity];
			for(int i = 0; i < size; i++) {
				temp[i] = array[i];
			}
			array = temp;
		}
		array[size++] = element;
		up();
	}
	public int getValue(int index) {
		if(index >= size) return 0;
		return array[index];
	}
	
	public void up() {
		int k = size;
		while(k > 1 && less(array[k / 2 - 1], array[k - 1])) {
			swap(k / 2 - 1, k - 1); 
			k = k / 2;
			
		}
	}
	private void swap(int index1, int index2) {
		int temp = array[index1];
		array[index1] = array[index2];
		array[index2] = temp;
	}
	private boolean less(int value1, int value2) {
		return value1 < value2;
	}
	public int getSize() {
		return size;
	}
	private void down() {
		int k = 0;
		int j;
		while((2 * k) < (size - 1)) {
			j = 2 * k + 1;
			if(j < (size - 1) && less(array[j], array[j+1])) j++;
			if(!less(array[k], array[j])) break;
			swap(k, j);
			k = j;
		}
	}
	public int remove() {
		if(size > 0) {
			int result = array[0];
			array[0] = array[--size];
			down();
			return result;
		}
		return 0;
	}
	
}
