package com.vg.ai.graph;

import java.util.Comparator;
import java.util.PriorityQueue;

public class PrimeMST {
	private boolean[] marked;
	PriorityQueue<Edge> pq = new PriorityQueue<Edge>();
	PriorityQueue<Edge> mst = new PriorityQueue<Edge>();
	public PrimeMST(EWUGraph g) {
		marked = new boolean[g.getSizeVertices()];
		visit(g, 0); //�������������� ��� 0 ����� ������� �������
		while(!pq.isEmpty()) {
			Edge e = pq.remove();
			int v1 = e.getV1();
			int v2 = e.getV2();
			if(marked[v1] && marked[v2]) continue;
			mst.add(e);
			if(!marked[v1]) visit(g, v1);
			if(!marked[v2]) visit(g, v2);
		}
	}
	private void visit(EWUGraph g, int v) {
		marked[v] = true;
		for(Edge e : g.getAdjacentE(v)) {
			if(!marked[e.getOther(v)]) {
				pq.add(e);
			}
		}
	}
	public Iterable<Edge> getEdges() {
		return mst;
	}
}
