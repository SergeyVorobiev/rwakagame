package com.vg.ai.graph;

public class DiEdge implements Comparable<DiEdge> {
	private int from;
	private int to;
	private double weight;
	public DiEdge(int from, int to, double weight) {
		this.from = from;
		this.to = to;
		this.weight = weight;
	}
	public int getFrom() {
		return from;
	}
	public int getTo() {
		return to;
	}
	public double getWeight() {
		return weight;
	}
	@Override
	public String toString() {
		return from + " - " + to + " (" + weight + ")"; 
	}
	public int getOther(int v) {
		if(v == from) return to;
		if(v == to) return from;
		throw new RuntimeException("Incorrect v: " + v);
	}
	@Override
	public int compareTo(DiEdge e) {
		if(this.getWeight() > e.getWeight()) return 1;
		if(this.getWeight() < e.getWeight()) return -1;
		return 0;
	}
	@Override
	public boolean equals(Object o) {
		DiEdge e = (DiEdge)o;
		if((this.getFrom() == e.getFrom() && this.getTo() == e.getTo()))  return true;
		return false;
	}
}