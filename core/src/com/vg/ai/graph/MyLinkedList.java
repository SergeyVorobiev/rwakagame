package com.vg.ai.graph;

import java.util.Iterator;

public class MyLinkedList<Value> implements Iterable<Value> {
	private class Node {
		private Node next = null;
		private Node prev = null;
		private Value value;
		public Node(Value value) {
			this.value = value;
		}
	}
	private int size = 0;
	private Node tail = null;
	private Node head = null;
	public void addToHead(Value value) {
		if(head == null) {
			head = new Node(value);
			tail = head;
		} else {
			Node newNode = new Node(value);
			newNode.next = head;
			head.prev = newNode;
			head = newNode;
		}
		size++;
	}
	public void addToTail(Value value) {
		if(tail == null) {
			tail = new Node(value);
			head = tail;
		} else {
			Node newNode = new Node(value);
			newNode.prev = tail;
			tail.next = newNode;
			tail = newNode;
		}
		size++;
	}
	public int getSize() {
		return size;
	}
	public Value getFirst() {
		if(size == 0) return null;
		return head.value;
	}
	public Value getLast() {
		if(size == 0) return null;
		return tail.value;
	}
	public Value deleteFirst() {
		if(size == 0) return null;
		size--;
		Node result = head;
		head = head.next;
		if(head == null) {
			tail = null;
		} else {
			head.prev = null;
		}
		result.next = null;
		return result.value;
	}
	public Value deleteLast() {
		if(size == 0) return null;
		size--;
		Node result = tail;
		tail = tail.prev;
		if(tail == null) {
			head = null;
		} else {
			tail.next = null;
		}
		result.prev = null;
		return result.value;
	}
	@Override 
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("{");
		Node curNode = head;
		while(curNode != null) {
			builder.append(curNode.value.toString());
			curNode = curNode.next;
			builder.append(", ");
		}
		if(builder.length() > 1) builder.setLength(builder.length() - 2);
		builder.append("} Size: " + size);
		return builder.toString();
	}
	@Override
	public Iterator<Value> iterator() {
		return new MyIterator();
	}
	private class MyIterator implements Iterator<Value> {
		private Node inext = head;
		private Node icur = null;
		@Override
		public boolean hasNext() {
			return inext != null;
		}

		@Override
		public Value next() {
			icur = inext;
			inext = inext.next;
			return icur.value;
		}
		@Override
		public void remove() {
			if(icur == null) return;
			size--;
			Node tprev = icur.prev;
			Node tnext = icur.next;
			icur.next = null;
			icur.prev = null;
			icur = null;
			if(tprev != null) {
				tprev.next = tnext;
			} else { //�� � ������, � ������ head ���������� ��������
				head = tnext;
			}
			if(tnext != null) {
				tnext.prev = tprev;
			} else { //�� � ������, � ������ tail ���������� ��������
				tail = tprev;
			}
		}
	}
}
