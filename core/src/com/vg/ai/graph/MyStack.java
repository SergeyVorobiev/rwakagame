package com.vg.ai.graph;

import java.util.Iterator;

public class MyStack<Value> implements Iterable<Value> {
	private MyLinkedList<Value> list = new MyLinkedList<Value>();
	public void add(Value value) {
		list.addToHead(value);
	}
	public int getSize() {
		return list.getSize();
	}
	public Value get() {
		return list.getFirst();
	}
	public Value delete() {
		return list.deleteFirst();
	}
	@Override
	public String toString() {
		return list.toString();
	}
	@Override
	public Iterator<Value> iterator() {
		return list.iterator();
	}
}
