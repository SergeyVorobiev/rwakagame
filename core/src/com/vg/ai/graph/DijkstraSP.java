package com.vg.ai.graph;

import java.util.Collection;
import java.util.Deque;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class DijkstraSP {
	private DiEdge[] edgeTo;
	private double[] distTo;
	private PriorityQueue<Integer> pq;

	public static void getPath(EWDiGraph g, int from, int to, LinkedList<DiEdge> collection) {
		int size = g.getSizeVertices();
		DiEdge[] edgeTo = new DiEdge[size];
		double[] distTo = new double[size];
		PriorityQueue<Integer> pq = new PriorityQueue(size);
		for (int i = 0; i < size; i++) distTo[i] = Double.POSITIVE_INFINITY;
		distTo[from] = 0.0;
		pq.add(from);
		while (!pq.isEmpty()) shortest(g, pq.remove(), distTo, edgeTo, pq);
		getPathTo(to, collection, distTo, edgeTo);

	}

	private static void shortest(EWDiGraph g, int v, double distTo[], DiEdge[] edgeTo, PriorityQueue<Integer> pq) {
		for(DiEdge e : g.getAdjacentE(v)) {
			int to = e.getTo();
			if(distTo[to] > distTo[v] + e.getWeight()) {
				distTo[to] = distTo[v] + e.getWeight();
				edgeTo[to] = e;
				if(!pq.contains(to)) pq.add(to); 
			}
		}
	}
	public static boolean hasPathTo(int to, double[] distTo) {
		if(to >= 0 && to < distTo.length)
			return distTo[to] != Double.POSITIVE_INFINITY;
		return false;
	}
	public double getDistance(int to) {
		return distTo[to];
	}

	public static void getPathTo(int to, LinkedList<DiEdge> list, double[] distTo, DiEdge[] edgeTo) {
		if(hasPathTo(to, distTo)) {
			for(int v = to;;) {
				DiEdge e = edgeTo[v];
				if(e == null) break;
				list.addFirst(e);
				v = e.getFrom();
			}
		}
	}
}
