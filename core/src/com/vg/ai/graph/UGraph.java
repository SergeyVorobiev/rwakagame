package com.vg.ai.graph;

import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;

public class UGraph implements IUGraph {
	private TreeMap<Integer, TreeSet<Integer>> map = new TreeMap<Integer, TreeSet<Integer>>();
	
	public void addEdge(int v1, int v2) {
		if(v1 == v2) return;
		TreeSet<Integer> treeV1 = map.get(v1);
		TreeSet<Integer> treeV2 = map.get(v2);
		if(treeV1 == null) {
			treeV1 = new TreeSet<Integer>();
			treeV1.add(v2);
			map.put(v1, treeV1);
		} else {
			treeV1.add(v2);
		}
		if(treeV2 == null) {
			treeV2 = new TreeSet<Integer>();
			treeV2.add(v1);
			map.put(v2, treeV2);
		} else {
			treeV2.add(v1);
		}
	}
	@Override
	public int getSizeVertices() {
		return map.size();
	}
	@Override
	public int getSizeEdges() {
		boolean[] marked = new boolean[map.size()];
		int size = 0;
		for(Entry<Integer, TreeSet<Integer>> entry : map.entrySet()) {
			marked[entry.getKey()] = true;
			for(Integer i : entry.getValue()) {
				if(!marked[i]) size++;
			}
		}
		return size;
	}
	@Override
	public Iterable<Integer> getAdjacent(int v) {
		return map.get(v);
	}
	@Override 
	public String toString() {
		StringBuilder builder = new StringBuilder();
		for(Entry<Integer, TreeSet<Integer>> entry : map.entrySet()) {
			builder.append("[" + entry.getKey() + "] ");
			builder.append("{");
			for(Integer i : entry.getValue()) {
				builder.append(i);
				builder.append(", ");
			}
			builder.setLength(builder.length() - 2);
			builder.append("}");
			builder.append("\n");
		}
		return builder.toString();
	}
}
