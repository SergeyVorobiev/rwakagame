package com.vg.ai.tasks;

import com.vg.professions.FreeWalker;

public class WalkTask extends TaskV2<FreeWalker> {

    public WalkTask() {
        super(10);
    }

    @Override
    protected void init() {
        addStep(FreeWalker.identifyPlace, FreeWalker.goToDest, FreeWalker.identifyPlace);
        addStep(FreeWalker.goToDest, FreeWalker.identifyPlace, FreeWalker.identifyPlace);
    }

    @Override
    public boolean isBlocked() {
        return false;
    }
}
