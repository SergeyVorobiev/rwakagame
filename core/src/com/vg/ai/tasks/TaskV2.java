package com.vg.ai.tasks;

import com.vg.professions.Behavior;

import java.util.ArrayList;

public abstract class TaskV2<T extends Behavior> {
    public final int type;
    private ArrayList<int[]> allSteps = new ArrayList<>();
    protected T behavior;
    private int currentAction;
    public TaskV2(int type) {
        this.type = type;
    }

    public void setBehavior(T behavior) {
        this.behavior = behavior;
        init();
        currentAction = 0;
    }

    protected void addStep(int inProgress, int done, int cannot) {
        allSteps.add(inProgress, new int[]{inProgress, done, cannot});
    }

    public boolean isFinished() {
        return currentAction == -1;
    }

    protected abstract void init();

    public void reset() {
        currentAction = 0;
    }

    public void update(float dt) {
        int[] results = allSteps.get(currentAction);
        currentAction = results[behavior.invokeAction(currentAction, dt)];
    }
    public abstract boolean isBlocked();
}
