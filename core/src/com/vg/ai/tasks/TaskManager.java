package com.vg.ai.tasks;

import com.badlogic.gdx.utils.Array;

public class TaskManager {

    // Not assigned tasks related to tiles in map, all entities will be see this tasks to assign.
    public final Array<TaskV2> accessibleTasks = new Array<>(1000);
    public final Array<TaskV2> blockedTasks = new Array<>(1000);

     // Assigned tasks.
     //public final Array<BringResourceTask> assignTasks = new Array<>(1000);

    public void update(float dt) {
        for (int i = 0; i < blockedTasks.size; i++) {
            if(!blockedTasks.get(i).isBlocked()) {
                TaskV2 task = blockedTasks.removeIndex(i);
                accessibleTasks.add(task);
            }
        }
    }

    public TaskV2 getLastTask() {
        int index = accessibleTasks.size - 1;
        if(index >= 0)
            return accessibleTasks.removeIndex(index);
        return null;

    }
}
