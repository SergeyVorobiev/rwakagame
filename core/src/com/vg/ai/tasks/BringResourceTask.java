package com.vg.ai.tasks;

import com.vg.objects.BuildingObject;
import com.vg.professions.Porter;

public class BringResourceTask extends TaskV2<Porter> {

	public final BuildingObject buildingObject;
	public final int resourceType;

	public BringResourceTask(BuildingObject buildingObject, int resourceType) {
		super(Task.BRING);
		this.buildingObject = buildingObject;
		this.resourceType = resourceType;
	}
	// Set the graph of states using method addStep.
	@Override
	protected void init() {
		/*
		addStep(behavior::askResource, behavior::canTakeResource, null);
		addStep(behavior::canTakeResource, behavior::findResource, behavior::hasResource);
		addStep(behavior::findResource, behavior::goToResource, behavior::hasResource);
		addStep(behavior::goToResource, behavior::tryTakeResource, behavior::findResource);
		addStep(behavior::tryTakeResource, behavior::canTakeResource, behavior::block);
		addStep(behavior::hasResource, behavior::findDestinationPlace, behavior::block);
		addStep(behavior::findDestinationPlace, behavior::goToObject, behavior::tryThrowOutResource);
		addStep(behavior::goToObject, behavior::putResource, behavior::block);
		addStep(behavior::putResource, behavior::askResource, behavior::block);
		addStep(behavior::tryThrowOutResource, behavior::block, behavior::block);
		*/
	}

	@Override
	public boolean isBlocked() {
		return false;
	}
}
