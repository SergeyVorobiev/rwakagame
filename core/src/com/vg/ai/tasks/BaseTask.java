package com.vg.ai.tasks;

public abstract class BaseTask implements Comparable<BaseTask> {
    /*
    public static final int BUILD = 0;
    public static final int BROKE = 1;
    public static final int WALK = 2;
    public static final int STATE_START = 0;
    public static final int STATE_PREPARE = 1;
    public static final int STATE_EXECUTING = 2;
    private int state = STATE_PREPARE;
    public int type;
    private final TaskManager taskManager;
    protected BaseTask parent = null;
    public float priority;
    public TileInfo targetTile;
    private boolean pause;
    protected DynamicObject executor;
    public BaseTask(int type, TileInfo targetTile) {
        this.type = type;
        this.taskManager = GameInstruments.taskManager;
        this.targetTile = targetTile;
    }

    final public void doTask(float dt) {
        switch (state) {
            case STATE_PREPARE:
                if (prepare(dt))
                    state = STATE_EXECUTING;
                break;
            case STATE_EXECUTING:
                if (execute(dt))
                    innerEnd();
                break;
        }
    }

    protected abstract boolean execute(float dt);
    protected abstract boolean prepare(float dt);
    protected abstract void endTask();
    private void innerEnd() {
        executor.task = null;
        taskManager.assignTasks.removeValue(this, false);
        endTask();
    }

    public boolean initTask(DynamicObject object) {
        state = STATE_START;
        executor = object;
        boolean result = start();
        state = STATE_PREPARE;
        return result;
    }
    public abstract boolean isBlocked();
    protected abstract boolean start();
    @Override
    public boolean equals(Object object) {
        BaseTask task = (BaseTask) object;
        if (this.targetTile == task.targetTile)
            return true;
        return false;
    }

    public void cancelAndDelete() {
        executor.finishTask();
        executor = null;
        taskManager.assignTasks.removeValue(this, false);
    }

    public void cancelAndReturn() {
        executor.finishTask();
        executor = null;
        taskManager.assignTasks.removeValue(this, false);
        taskManager.tasks.add(this);
    }

    public void pauseOn() {
        if (pause)
            return;
        pause = true;
        TaskManager manager = GameInstruments.taskManager;
        if (executor != null) {
            executor.finishTask();
            manager.assignTasks.removeValue(this, false);
        } else {
            if(!manager.tasks.removeValue(this, false))
                manager.blocked.removeValue(this, false);
        }
        executor = null;
        // TODO: add container for paused tasks.

    }

    public void pauseOff() {
        if(!pause)
            return;
        pause = false;
        TaskManager manager = GameInstruments.taskManager;
        // manager.paused.removeValue(this, false);
        manager.tasks.add(this);
    }

    protected void block() {
        executor.finishTask();
        executor = null;
        if(state != STATE_START)
            GameInstruments.taskManager.assignTasks.removeValue(this, false);
        else
            GameInstruments.taskManager.tasks.removeValue(this, false);
        GameInstruments.taskManager.blocked.add(this);
    }
    */
}
