package com.vg.ai.tasks;

import com.vg.professions.Builder;

public class BuildTask extends TaskV2<Builder> {
    public BuildTask() {
        super(Task.BUILD);
    }

    @Override
    protected void init() {
        /*
        addStep(behavior::findBuildPlace, behavior::goToBuilding, behavior::block);
        addStep(behavior::goToBuilding, behavior::build, behavior::findBuildPlace);
        addStep(behavior::build, null, behavior::block);
        */
    }

    @Override
    public boolean isBlocked() {
        return false;
    }
}
