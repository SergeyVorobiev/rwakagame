package com.vg.ai.tasks;

public interface Task<T> {
    int BRING = 0;
    int BUILD = 1;
    boolean execute(T object, float dt);
    void block(T object, float time);
    void interrupt(T object);
    void finish(T object);
    boolean isBlocked(float dt);
    void reset();
}
