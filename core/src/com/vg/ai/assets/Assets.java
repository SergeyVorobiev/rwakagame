package com.vg.ai.assets;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;

public class Assets {
    public static final Texture texture = getT("collector2");
    public static final Texture BRICK = new Texture("material.png");
    public static final Texture collector = new Texture("tiles/collector.png");
    public static final Texture collector2 = new Texture("tiles/collector2.png");
    public static final Texture red = new Texture("tiles/1.png");
    public static final Texture brown = new Texture("tiles/2.png");
    public static final Texture blue = new Texture("tiles/3.png");
    public static final Texture yellow = new Texture("tiles/5.png");
    public static final Texture green = new Texture("tiles/6.png");
    public static final Texture buildIcon = new Texture("tiles/build_icon.png");
    public static final Texture factory = new Texture("tiles/repair.png");
    public static final Texture repair_menu = new Texture("tiles/repair_menu.png");
    public static final Texture wall_menu = new Texture("tiles/wall_menu.png");
    public static final Texture wall = getT("wall");
    public static Texture getT(String name) {
        return new Texture("tiles/" + name + ".png");
    }
    public static final Sprite[] sprites;
    static {
        sprites = new Sprite[2];
        sprites[0] = new Sprite(wall);
        sprites[1] = new Sprite(factory);
    }
}
