package com.vg.ai.assets;

import com.badlogic.gdx.graphics.Texture;
import com.vg.ai.search.TileInfo;

public class MaterialAssets {
    public static final int BRICK = 1;
    public static final Texture[] materials = new Texture[10];
    public static final int[] mass = new int[10];
    public static final int[] rockStrength = new int[10];
    public static void setMaterial(TileInfo info, int count, int type) {
        info.materialType = type;
        info.materialCount = count;
    }
    static {
        materials[BRICK] = new Texture("material.png");
        rockStrength[1] = 1000;
        rockStrength[2] = 2000;
        rockStrength[3] = 3000;
    }
}
