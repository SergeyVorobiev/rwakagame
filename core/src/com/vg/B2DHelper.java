package com.vg;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.*;

public class B2DHelper {
    private static BodyDef bodyDef = new BodyDef();
    private static FixtureDef fixtureDef = new FixtureDef();
    private static PolygonShape pShape = new PolygonShape();
    public static void createBody(World world, Rectangle r) {
        bodyDef.type = BodyDef.BodyType.StaticBody;
        bodyDef.position.set(r.getX() + r.getWidth() / 2, r.getY() + r.getHeight() / 2);
        pShape.setAsBox(r.getWidth() / 2, r.getHeight() / 2);
        fixtureDef.shape = pShape;
        world.createBody(bodyDef).createFixture(fixtureDef);
    }
}
