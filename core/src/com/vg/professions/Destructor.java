package com.vg.professions;

public interface Destructor extends Walker {
    int findDestructPlace(float dt);
    int destruct(float dt);
}
