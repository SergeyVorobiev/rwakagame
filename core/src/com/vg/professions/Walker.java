package com.vg.professions;

import com.vg.ai.search.TileInfo;

public interface Walker extends Behavior {
    int goToDestination(float dt);
    int findClosesPlace(TileInfo[] places, float dt);
}
