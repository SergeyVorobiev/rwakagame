package com.vg.professions;

public interface Action {
    static final int IN_PROGRESS = 0;
    static final int DONE = 1;
    static final int CANNOT = 2;
    int execute(float dt);
}