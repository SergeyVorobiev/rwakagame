package com.vg.professions;

public interface Builder extends Behavior {
    int findBuildPlace(float dt);
    int goToBuilding(float dt);
    int build(float dt);
}
