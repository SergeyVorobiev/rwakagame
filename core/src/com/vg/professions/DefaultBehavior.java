package com.vg.professions;

public class DefaultBehavior implements Behavior {

    @Override
    public int block(float dt) {
        return 0;
    }

    @Override
    public int invokeAction(int action, float dt) {
        return 0;
    }

}
