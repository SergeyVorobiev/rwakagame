package com.vg.professions;

import com.vg.objects.BuildingObject;
import com.vg.objects.DynamicObject;

public class DefaultBuilder extends DefaultWalker implements Builder {
    private BuildingObject buildingObject;
    public DefaultBuilder(DynamicObject dyn) {
        super(dyn);
    }

    public void init(BuildingObject buildingObject) {
        this.buildingObject = buildingObject;
    }
    @Override
    public int findBuildPlace(float dt) {
        return this.findClosesPlace(buildingObject.aroundTiles, dt);
    }

    @Override
    public int goToBuilding(float dt) {
        return this.goToDestination(dt);
    }

    @Override
    public int build(float dt) {
        return buildingObject.build(dt * 1000) ? Action.DONE : Action.IN_PROGRESS;
    }
}
