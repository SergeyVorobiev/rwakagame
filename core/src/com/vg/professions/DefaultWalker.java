package com.vg.professions;

import com.vg.ai.search.PathTracker;
import com.vg.ai.search.TileInfo;
import com.vg.objects.DynamicObject;

public class DefaultWalker extends DefaultBehavior implements Walker {

    protected PathTracker tracker;
    protected DynamicObject dyn;
    public int curIndex;
    public TileInfo curPlaceToGo;
    public DefaultWalker(DynamicObject dyn) {
        this.tracker = dyn.tracker;
        this.dyn = dyn;
    }

    @Override
    public int findClosesPlace(TileInfo[] places, float dt) {
        setClosesTileToGo(places);
        return curPlaceToGo == null ? Action.CANNOT : Action.DONE;
    }

    @Override
    public int goToDestination(float dt) {
        int status = tracker.goToPlace(curPlaceToGo);
        switch (status) {
            case PathTracker.CANNOT_REACH:
                return Action.CANNOT;
            case PathTracker.MOVING:
                dyn.move(dt);
                return Action.IN_PROGRESS;
            case PathTracker.SEARCHING_PATH:
                return Action.IN_PROGRESS;
            case PathTracker.IN_POSITION:
                return Action.DONE;
            default:
                throw new RuntimeException("Wrong status of tracker.");
        }
    }

    protected void setClosesTileToGo(TileInfo[] tiles) {
        curIndex++;
        curPlaceToGo = null;
        for(; curIndex < tiles.length; curIndex++) {
            TileInfo info = tiles[curIndex];
            if(info == null) {
                return;
            }
            if(info.getK() < 0)
                continue;
            DynamicObject dyn = info.getPlacedObject();
            if(dyn != null && dyn != this.dyn)
                continue;
            curPlaceToGo = info;
            break;
        }
    }

    @Override
    public int invokeAction(int action, float dt) {
        return 0;
    }
}
