package com.vg.professions;

import com.vg.ai.search.TileInfo;
import com.vg.objects.BuildingObject;
import com.vg.objects.DynamicObject;
import com.vg.storage.MaterialStorage;

public class DefaultPorter extends DefaultWalker implements Porter {
    public int neededResourceType;
    private int countOfResourceToTake;
    public TileInfo[] objectPlaces;
    public TileInfo[] resourcePlaces;

    public int carry;
    public int carryCapacity;
    private BuildingObject buildingObject;
    public DefaultPorter(DynamicObject dyn) {
        super(dyn);
    }

    public void init(int carry, int carryCapacity, int neededResourceType, BuildingObject object) {
        this.carryCapacity = carryCapacity;
        this.carry = carry;
        buildingObject = object;
        this.neededResourceType = neededResourceType;
        resourcePlaces = MaterialStorage.getTilesWithMaterial(neededResourceType, null);
        objectPlaces = object.aroundTiles;
    }

    @Override
    public int askResource(float dt) {
        int count = buildingObject.getNeedResource(neededResourceType);
        if(count <= 0)
            return Action.CANNOT;
        countOfResourceToTake = count > carryCapacity ? carryCapacity : count;

        // It will be increased in getClosedTileToGo().
        curIndex = -1;
        TileInfo[] resourceTiles = MaterialStorage.getTilesWithMaterial(neededResourceType, null);
        resourcePlaces = tracker.getTilesSortedByDistance(resourceTiles, null);
        return Action.DONE;
    }

    @Override
    public int canTakeResource(float dt) {
        return countOfResourceToTake > 0 ? Action.DONE : Action.CANNOT;
    }

    @Override
    public int findResource(float dt) {
        return findClosesPlace(resourcePlaces, dt);
    }

    @Override
    public int findDestinationPlace(float dt) {
        return findClosesPlace(objectPlaces, dt);
    }

    @Override
    public int tryThrowOutResource(float dt) {
        return 0;
    }

    @Override
    public int goToObject(float dt) {
        return goToDestination(dt);
    }


    @Override
    public int goToResource(float dt) {
        return goToDestination(dt);
    }

    @Override
    public int tryTakeResource(float dt) {
        if(neededResourceType == -1)
            throw new RuntimeException("Wrong type of resource to take.");
        if(neededResourceType == curPlaceToGo.materialType) {
            int countToTake = countOfResourceToTake > curPlaceToGo.materialCount ? curPlaceToGo.materialCount :
                    countOfResourceToTake;
            countOfResourceToTake -= countToTake;
            MaterialStorage.removeFromStorage(curPlaceToGo, countToTake);
            carry += countToTake;
        }
        return Action.DONE;
    }

    @Override
    public int hasResource(float dt) {
        int result = carry > 0 ? Action.DONE : Action.CANNOT;

        // Now we need to sort all tiles of object by distance.
        this.objectPlaces = tracker.getTilesSortedByDistance(buildingObject.aroundTiles, null);
        curIndex = -1;
        return result;
    }

    @Override
    public int putResource(float dt) {
        buildingObject.addNeedResource(carry, neededResourceType);
        carry = 0;
        return Action.DONE;
    }
}