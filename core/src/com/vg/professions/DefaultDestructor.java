package com.vg.professions;

import com.vg.GameInstruments;
import com.vg.ai.assets.MaterialAssets;
import com.vg.ai.search.TileInfo;
import com.vg.objects.DynamicObject;
import com.vg.storage.MaterialStorage;

public class DefaultDestructor extends DefaultWalker implements Destructor {
    private int progress;
    private int strength;
    private TileInfo place;
    public DefaultDestructor(DynamicObject dyn, TileInfo place) {
        super(dyn);
        this.place = place;
        this.progress = 0;
        this.strength = 4000;
    }

    @Override
    public int findDestructPlace(float dt) {
        return findClosesPlace(place.roundTiles, dt);
    }

    @Override
    public int destruct(float dt) {
        progress += dt * 1000;
        int action = progress > strength ? Action.DONE : Action.IN_PROGRESS;
        if(action == Action.DONE) {
            GameInstruments.deleteCell(place.x, place.y, 1);
            place.setK(1);
            MaterialStorage.addToStorage(place, 20, MaterialAssets.BRICK);
        }
        return action;
    }

    @Override
    public int block(float dt) {
        return 0;
    }
}
