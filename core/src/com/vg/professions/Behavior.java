package com.vg.professions;

public interface Behavior {
    int block(float dt);
    int invokeAction(int action, float dt);
}
