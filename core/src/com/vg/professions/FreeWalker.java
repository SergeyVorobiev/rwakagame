package com.vg.professions;

import com.vg.GameInstruments;
import com.vg.ai.search.TileInfo;
import com.vg.ai.search.TileInfoBuilder;
import com.vg.objects.DynamicObject;

import java.util.Random;

public class FreeWalker extends DefaultWalker {
    public static final int goToDest = 1;
    public static final int identifyPlace = 0;
    private Random rnd = new Random();
    private float time = 0;
    private float timeToStandUp = 0;
    private TileInfo[] place = new TileInfo[1];
    private TileInfo[] variants = new TileInfo[1000];
    public FreeWalker(DynamicObject dyn) {
        super(dyn);
    }

    public int identifyPlace(float dt) {
        getPlace(dt);
        if(place[0] != null) {
            this.curIndex = -1;
            return this.findClosesPlace(place, dt);
        }
        return Action.IN_PROGRESS;
    }

    private void getPlace(float dt) {
        place[0] = null;
        time += dt;
        if (time > timeToStandUp) {
            time = 0;
            int decision = rnd.nextInt(2);
            if (decision == 0) {
                timeToStandUp = rnd.nextInt(2) + 1;
            } else {
                int radius = rnd.nextInt(10) + 5;
                TileInfoBuilder.getTilesAroundAsCube(GameInstruments.getTiles(), tracker.getTargetTileX(),
                        tracker.getTargetTileY(), radius, variants);
                int k = 0;
                for (int i = 0; i < variants.length; i++) {
                    TileInfo tile = variants[i];
                    if (tile == null)
                        break;
                    if (tile.getPlacedObject() == null && tile.getK() > 0)
                        variants[k++] = tile;
                }
                if (k > 0) {
                    place[0] = variants[rnd.nextInt(k)];
                }
            }
        }
    }

    @Override
    public int invokeAction(int action, float dt) {
        switch(action) {
            case goToDest:
                return goToDestination(dt);
            case identifyPlace:
                return identifyPlace(dt);
            default:
                throw new RuntimeException("sdf");
        }
    }
}
