package com.vg.professions;

public interface Porter extends Behavior {
	int askResource(float dt);
	int canTakeResource(float dt);
	int findResource(float dt);
	int findDestinationPlace(float dt);
	int tryThrowOutResource(float dt);
	int goToResource(float dt);
	int tryTakeResource(float dt);
	int hasResource(float dt);
	int goToObject(float dt);
	int putResource(float dt);
}
