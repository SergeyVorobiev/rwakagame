package com.vg;

public class Timer {
	private static long start;
	public static void start() {
		start = System.nanoTime();
	}
	public static double stop() {
		return (System.nanoTime() - start) / 1000000000.0; 
	}
	public static double stopAndPrint() {
		double time = stop();
		System.out.println(time);
		return time;
	}

	public static void sleep(int millis) {
		try {
			Thread.currentThread().sleep(millis);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
