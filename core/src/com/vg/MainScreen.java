package com.vg;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.utils.Array;
import com.vg.ai.assets.Assets;
import com.vg.ai.assets.MaterialAssets;
import com.vg.ai.graph.OrthoTileCameraDriver;
import com.vg.ai.search.AreaSearch;
import com.vg.ai.search.TileInfo;
import com.vg.input.GameInput;
import com.vg.objects.BuildingObject;
import com.vg.objects.DynamicObject;
import com.vg.storage.ObjectStorage;
import com.vg.ui.MainMenu;
import com.vg.ui.StatusUI;

import java.util.concurrent.Callable;

public class MainScreen implements Screen {

    private World world;
    //private Box2DDebugRenderer bRenderer = new Box2DDebugRenderer();
    private MyBody mBody;
    private ShapeRenderer shapeRenderer;
    private TextureAtlas textureAtlas;
    private Animation<TextureRegion> animation;
    private SpriteBatch spriteBatch = new SpriteBatch();
    private TextureRegion region;
    private TileInfo[] pathCoords = new TileInfo[300 * 300];
    private TileInfo[] explode = new TileInfo[200];
    private Array<DynamicObject> dyns = new Array<>(10);
    private int dynCount = 1;
    private OrthographicCamera camera;
    private WorldMetric metric;
    public StatusUI stat;
    private OrthogonalTiledMapRenderer orthoTiles;
    private GameInstruments gameInstruments;
    private AreaSearch as = new AreaSearch();
    private MainMenu menu;
    private GameInput gameInput;
    private OrthoTileCameraDriver camDriver;

    public MainScreen(GameInstruments gameInstruments) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        camera = gameInstruments.camDriver.getCamera();
        metric = gameInstruments.metric;
        shapeRenderer = new ShapeRenderer();
        region = new TextureRegion(Assets.texture);
        this.gameInstruments = gameInstruments;
        orthoTiles = gameInstruments.orthoTiles;
        camDriver = GameInstruments.camDriver;
        loadObjectTextures();
        initPhysicsForMap();
        initDynamicObjects();
//        material = new Texture("material.png");
//        t5 = new Texture("tiles/5.png");
//        t6 = new Texture("tiles/6.png");
//        t7 = new Texture("tiles/7.png");
        Skin skin =  new Skin(Gdx.files.internal("sprites/test/ui.json"), new TextureAtlas("sprites/test/ui.atlas"));
        Window.WindowStyle wStyle = new Window.WindowStyle();

        wStyle.titleFont = skin.getFont("default-font");
        skin.add("default", wStyle);
        InputMultiplexer multiplexer = new InputMultiplexer();


       // skin.add("default", new BitmapFont());
        /*FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("sprites/test/calibri.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter par = new FreeTypeFontGenerator.FreeTypeFontParameter();
        BitmapFont bfont = gen.generateFont(par);
        gen.dispose();
        skin.add("default-font", bfont);*/


        //, metric.widthInMeters / 2, metric.heightInMeters / 2);
        //Stage stage = new Stage(new StretchViewport(metric.vWidthInMeters, metric.vHeightInMeters));
       // stat = new StatusUI("sf", skin, stage);
        //multiplexer.addProcessor(stage);
        menu = new MainMenu();
        multiplexer.addProcessor(menu.stage);
        gameInput = new GameInput();
        multiplexer.addProcessor(gameInput);
/*
        Gdx.input.setInputProcessor(new InputAdapter() {
            public boolean touchDown(int screenX, int screenY, int pointer, int button) {
                L.pr("hs");
                return true;
            }
        });*/
        Gdx.input.setInputProcessor(multiplexer);
    }

    private void loadObjectTextures() {
        textureAtlas = new TextureAtlas("sprites/test/collector.txt");
        Array<TextureAtlas.AtlasRegion> regions = textureAtlas.findRegions("collector");
        animation = new Animation<>(0.1f, regions);
        animation.setPlayMode(Animation.PlayMode.LOOP);
    }

    private void initDynamicObjects() {
        mBody = new MyBody(BodyFactory.createDynamicCircleBody(world, 1, 3, 1));
        for(int i = 0; i < dynCount; i++)
            dyns.add(new DynamicObject(i, i));
        //dyn = new DynamicObject(new PathTracker(f, 0, 0, metric));
    }

    private void initPhysicsForMap() {
        world = new World(new Vector2(0, 0), true);
        world.setGravity(new Vector2(0, -10));
        initPhysicsForLayer(2);
    }

    private void initPhysicsForLayer(int layerNumber) {
        getRectanglesForLayer(layerNumber)
                .forEach((rmo) -> B2DHelper.createBody(world, metric.convertToMeters(rmo.getRectangle())));
    }

    private Array<RectangleMapObject> getRectanglesForLayer(int layerNumber) {
        return orthoTiles.getMap().getLayers().get(layerNumber).getObjects().getByType(RectangleMapObject.class);
    }

    @Override
    public void show() {
        L.p(1);
    }

    @Override
    public void render(float delta) {
        clear();
        handleInput(delta);
        drawWorld(delta);
    }

    private void handleInput(float delta) {
        mBody.handleInput();
        //camDriver.getCamera().position.y = mBody.getPosY();
        camDriver.handleDrive(delta);
        mouseMoved();
    }

    private void mouseMoved() {
        BuildingObject buildingObject = ObjectStorage.objectByHand;
        if(buildingObject != null) {
            int tileX = camDriver.getXTileNumber(Gdx.input.getX());
            int tileY = camDriver.getYTileNumber(Gdx.input.getY());
            buildingObject.setTilePos(tileX, tileY);
        }
    }

    private Callable<Boolean> runArea = () -> {
            //Timer.start();
            GameInstruments.searchEngine.initClosedAreas(as);
            //Timer.stopAndPrint();
            Thread.sleep(1000);
                return true;
};
    private void drawWorld(float delta) {
        orthoTiles.setView(camera);
        //Timer.start();
        //orthoTiles.render();
        //Timer.stopAndPrint();
        //world.step(delta, 1, 1);
        //bRenderer.render(world, camDriver.getCamera().combined);
        spriteBatch.begin();
        spriteBatch.setProjectionMatrix(camera.combined);
        //TextureRegion r = animation.getKeyFrame(elapsed += delta);

        // Draw closed areas.
        if(GameInstruments.exec.getActiveCount() == 0) {
            GameInstruments.exec.submit(runArea);
        }
        drawArea();

        // Draw materials.
        TileInfo[][] infoss = GameInstruments.searchEngine.getTiles();
        for (int x = 0; x < metric.widthInTiles; x++) {
            TileInfo[] infos = infoss[x];
            for (int y = 0; y < metric.heightInTiles; y++) {
                TileInfo info = infos[y];
                if(info.materialType != -1) {
                    drawOneTile(MaterialAssets.materials[info.materialType], spriteBatch, info.x, info.y);
                }
            }
        }

        // Draw buildings.
        Array<BuildingObject> buildings = ObjectStorage.buildings;
        for (int i = 0; i < buildings.size; i++) {
            BuildingObject object = buildings.get(i);
            object.drawer.draw(spriteBatch, object);
        }
        spriteBatch.setColor(1, 1, 1, 1);
        gameInstruments.taskManager.update(delta);
        for(DynamicObject dyn: dyns) {
            dyn.update(delta);
            spriteBatch.draw(dyn.getView(delta), dyn.getPosX(), dyn.getPosY(), 1, 1f);
        }
        /*
        for(DynamicObject dyn : dyns) {
            dyn.move(delta);
            spriteBatch.draw(texture, dyn.getPosX(), dyn.getPosY(), 2, 2);
        }
        drawInTileCenter(material, spriteBatch,3, 3, 1.5f, 1.5f);
        /*
        for(TileInfo info : explode) {
            if(info == null)
                break;
            spriteBatch.draw(texture, info.x * metric.tileSizeInMeters, info.y * metric.tileSizeInMeters, 2, 2);
        }*/
        /*
        for(int i = 0; i < explode.length; i++) {
            TileInfo info = explode[i];
            if(info == null) break;
            draw(texture, spriteBatch, info.x, info.y);
        }*/
        //runThroughArrayAndDraw(pathCoords);

        spriteBatch.end();
        shapeRenderer.setProjectionMatrix(camera.combined);
        shapeRenderer.setColor(0, 0, 1, 1);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        for (DynamicObject dyn : dyns) {
            dyn.drawPath(shapeRenderer);
        }

        shapeRenderer.end();
        menu.draw();

    }

    private void drawArea() {
        synchronized (as.block) {
            Timer.start();
            int startX = camDriver.getXTileNumber(0);
            startX = startX < 0 ? 0 : startX;
            int endX =  camDriver.getXTileNumber(1200) + 1;
            int endY = camDriver.getYTileNumber(0) + 1;
            int startY = camDriver.getYTileNumber(650);
            startY = startY < 0 ? 0 : startY;
            endX = endX > 299 ? 299 : endX;
            endY = endY > 299 ? 299 : endY;
            //startX = 3;
            //startY = 10;
            //endX = 30;
            //endY = 15;
            //zzzzzzzzzzzzzzL.pr(startX + " " + endX);
            for (int x = startX; x < endX; x++) {
                int[] xArea = as.area[x];
                for (int y = startY; y < endY; y++) {
                    int r = xArea[y];
                    if (r == 0)
                        spriteBatch.draw(Assets.brown, x, y, 1, 1);
                    else if (r == 1)
                        spriteBatch.draw(Assets.blue, x, y, 1, 1);
                    else if (r == 2)
                        spriteBatch.draw(Assets.yellow, x, y, 1, 1);
                    else if (r == 3)
                        spriteBatch.draw(Assets.green, x, y, 1, 1);
                }
            }
            Timer.stopAndPrint();
        }
    }
    private void runThroughArrayAndDraw(TileInfo[] arr) {
        for(int i = 0; i < arr.length; i++) {
            TileInfo info = arr[i];
            if(info == null)
                break;
            drawOneTile(Assets.texture, spriteBatch, info.x, info.y);
        }
    }
    private void drawOneTile(Texture texture, Batch batch, int tileX, int tileY) {
        batch.draw(texture, tileX, tileY, 1, 1);
    }

    private void drawInTileCenter(Texture texture, Batch batch, int tileX, int tileY, float width, float height) {
        tileX += (1 - width) / 2.0f;
        tileY += (1 - height) / 2.0f;
        batch.draw(texture, tileX, tileY, width, height);
    }
    private void clear() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
    }

    @Override
    public void resize(int width, int height) {
        L.p();
    }

    @Override
    public void pause() {
        L.p();
    }

    @Override
    public void resume() {
        L.p();
    }

    @Override
    public void hide() {
        L.p();
    }

    @Override
    public void dispose() {
        L.p();
        gameInstruments.orthoTiles.dispose();
        //bRenderer.dispose();
        world.dispose();
        textureAtlas.dispose();
        spriteBatch.dispose();
        gameInstruments.searchExecutor.shutdown();
        gameInstruments.exec.shutdown();
    }
}
