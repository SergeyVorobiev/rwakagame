package com.vg;

import com.badlogic.gdx.graphics.g2d.Batch;

public interface Drawer<T> {
    void draw(Batch batch, T object);
}
