package com.vg;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.physics.box2d.*;

public class MyBody {
    private Body body;
    public MyBody(Body body) {
        this.body = body;
    }
    public void handleInput() {
        if(Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            body.applyLinearImpulse(0, 5, body.getLocalCenter().x, body.getLocalCenter().y, true);
        }
    }

    public float getPosX() {
        return body.getPosition().x;
    }

    public float getPosY() {
        return body.getPosition().y;
    }
}
