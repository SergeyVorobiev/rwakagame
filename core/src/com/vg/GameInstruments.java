package com.vg;

import com.badlogic.gdx.maps.MapLayers;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.vg.ai.graph.OrthoTileCameraDriver;
import com.vg.ai.search.SearchEngine;
import com.vg.ai.search.TileInfo;
import com.vg.ai.search.TileInfoBuilder;
import com.vg.ai.tasks.TaskManager;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

public class GameInstruments {
    public static final float CAM_WIDTH_IN_TILES = 26;
    public static final int WORLD_WIDTH_IN_TILES = 300;
    public static int WORLD_HEIGHT_IN_TILES = 300;
    public static float TILE_WIDTH_IN_PIXELS = 32;
    public static WorldMetric metric;
    public static MapLayers mapLayers;
    public static OrthogonalTiledMapRenderer orthoTiles;
    public static OrthoTileCameraDriver camDriver;
    public static final SearchEngine searchEngine;
    public static TaskManager taskManager;
    public static Random rnd = new Random();
    public static ExecutorService searchExecutor = Executors.newFixedThreadPool(1);
    public static ThreadPoolExecutor exec = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);

    static {
        metric =
                new WorldMetric(TILE_WIDTH_IN_PIXELS, WORLD_WIDTH_IN_TILES, WORLD_HEIGHT_IN_TILES,
                        CAM_WIDTH_IN_TILES, 16 / 9.0f);
        orthoTiles = new OrthogonalTiledMapRenderer(new TmxMapLoader().load("test.tmx"), metric.metersInPixel);
        mapLayers = orthoTiles.getMap().getLayers();
        camDriver = new KeyCameraDriver(metric);
        TiledMapTileLayer layer = (TiledMapTileLayer) mapLayers.get(1);
        taskManager = new TaskManager();
        searchEngine = new SearchEngine(TileInfoBuilder.getNewTileMap(layer));
    }

    public static TileInfo[][] getTiles() {
        return searchEngine.getTiles();
    }

    public static TiledMapTileLayer.Cell getCell(int x, int y, int layer) {
        return ((TiledMapTileLayer) mapLayers.get(layer)).getCell(x, y);
    }
    public static void setCell(int x, int y, int layer, TiledMapTileLayer.Cell cell) {
        ((TiledMapTileLayer) mapLayers.get(layer)).setCell(x, y, cell);
    }

    public static void deleteCell(int x, int y, int layer) {
        setCell(x, y, layer, null);
    }

    public static boolean isInBoundsX(int x) {
        if (x < 0 || x > getTiles().length - 1)
            return false;
        return true;
    }

    public static boolean isInBoundsY(int y) {
        if (y < 0 || y > getTiles()[0].length - 1)
            return false;
        return true;
    }
}
