package com.vg.objects;

import com.vg.DrawSelector;
import com.vg.Drawer;
import com.vg.ai.search.TileInfo;
import com.vg.ai.tasks.TaskV2;

public class GameObject<T> {

    // Width of object in tiles.
    public int tileWidth;

    // Height of object in tiles.
    public int tileHeight;

    // Tile represents first tile on which object is placed. And also it is single tile for 1x1 objects.
    public TileInfo startTile;

    // Tiles on which this object is placed. If object has 1x1 dimension then this is null because it one tile is
    // contained in startTile variable.
    public final TileInfo[] area;

    public final int type;

    // Strength of this object.
    public final float strength;

    public Drawer<T> drawer;

    public int state = Integer.MIN_VALUE;

    public TaskV2 task;

    public final TileInfo[] aroundTiles;

    protected DrawSelector<T> selector;
    public GameObject(DrawSelector<T> selector, int tileWidth, int tileHeight, float strength, int type) {
        this(selector,null, tileWidth, tileHeight, strength, type);
    }

    public GameObject(DrawSelector<T> selector, TileInfo startTile, int tileWidth, int tileHeight, float strength, int
            type) {
        this.selector = selector;
        this.tileWidth = tileWidth;
        this.tileHeight = tileHeight;
        this.area = new TileInfo[tileWidth * tileHeight];
        this.strength = strength;
        this.type = type;
        this.startTile = startTile;
        int aroundSize = (tileWidth + 2) * 2 + tileHeight * 2;
        this.aroundTiles = new TileInfo[aroundSize];
    }

    protected void setState(int state) {
        this.state = state;
        drawer = selector.getCurrentDrawer(state);
    }
}
