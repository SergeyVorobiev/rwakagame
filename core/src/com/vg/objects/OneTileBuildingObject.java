package com.vg.objects;

import com.vg.DrawSelector;

public class OneTileBuildingObject extends BuildingObject {

    // needResources - this is array contains types of resources and its counts with next order:
    // first is resource type, second resource count and so on for example: 0, 10, 2, 20 means 0 - type, 10 - count,
    // 2 - type, 20 - count.
    public OneTileBuildingObject(DrawSelector<BuildingObject> selector, float strength, int type, int[] needResources) {
        super(selector, 1, 1, strength, type, needResources);
    }
}
