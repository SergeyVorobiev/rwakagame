package com.vg.objects;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.utils.Array;
import com.vg.GameInstruments;
import com.vg.ai.search.PathTracker;
import com.vg.ai.search.TileInfo;
import com.vg.ai.tasks.Task;
import com.vg.ai.tasks.WalkTask;
import com.vg.professions.Behavior;
import com.vg.professions.DefaultPorter;
import com.vg.professions.FreeWalker;

import java.util.TreeMap;

public class DynamicObject extends GameObject {
    private float speed = 10;
    public final PathTracker tracker;
    public int carryCapacity;
    public boolean manualMode = false;
    public int carry;
    private float lastTime = 0;
    public Animation<TextureRegion> animation;
    private TreeMap<Integer, Behavior> behaviors = new TreeMap<>();
    private int behaviorType = Task.BRING;
    private Behavior curBehavior;
    public DynamicObject(int tilePosX, int tilePosY) {
        super(null, GameInstruments.searchEngine.getTileInfo(tilePosX, tilePosY), 1, 1, 100, 0);
        this.tracker = new PathTracker(this);
        this.carryCapacity = 100;
        Array<TextureAtlas.AtlasRegion> regions = new TextureAtlas("sprites/test/collector.txt").findRegions
                ("collector");
        animation = new Animation<>(0.1f, regions);
        animation.setPlayMode(Animation.PlayMode.LOOP);
        behaviors.put(Task.BRING, new DefaultPorter(this));
        curBehavior = behaviors.get(behaviorType);
    }

    public TileInfo getTargetTile() {
        return GameInstruments.searchEngine.getTileInfo(tracker.getTargetTileX(), tracker.getTargetTileY());
    }

    public float getPosX() {
        return tracker.getPosX();
    }

    public float getPosY() {
        return tracker.getPosY();
    }

    public void move(float dt) {
        tracker.move(speed, dt);
    }

    public void update(float dt) {
        boolean result = false;
        if(task == null) {
            //task = GameInstruments.taskManager.getLastTask();
            task = new WalkTask();
            task.setBehavior(new FreeWalker(this));
            //BringResourceTask myTask = (BringResourceTask) task;
            //DefaultPorter defPorter = (DefaultPorter) curBehavior;
            //defPorter.init(carry, carryCapacity, myTask.resourceType, myTask.buildingObject);
        }
        if (task != null) {
            if (task.isFinished()) {
                task = null;
            }
            task.update(dt);
        }
       // else if(!manualMode)
       //     assignTask(new WalkTask());
    }

    private boolean direction = false;
    public TextureRegion getView(float dt) {
        if(tracker.getPosX() < tracker.getTargetTileX())
            direction = true;
        else if(tracker.getPosX() > tracker.getTargetTileX())
            direction = false;
        TextureRegion r = animation.getKeyFrame(lastTime += dt);
        if(direction && r.isFlipX())
            r.flip(true, false);
        else if(!direction && !r.isFlipX())
            r.flip(true, false);
        return r;
    }

    public void drawPath(ShapeRenderer renderer) {
        synchronized(tracker.blocked) {
            if(tracker.getCurrentStatus() != PathTracker.MOVING)
                return;
            TileInfo[] path = tracker.pathCoords;
            boolean key = true;
            if(path[0] == null)
                return;
            int i = 1;
            for(; i < path.length; i++) {
                if(path[i] == null)
                    break;
                if(path[i] == getTargetTile()) {
                    key = false;
                    renderer.line(path[i].x + 0.5f, path[i].y + 0.5f, tracker.getPosX() + 0.5f, tracker.getPosY()
                            + 0.5f);
                    continue;
                }
                if(key) {
                    continue;
                }
                renderer.line(path[i].x + 0.5f, path[i].y + 0.5f, path[i - 1].x + 0.5f, path[i - 1].y + 0.5f);
            }
            renderer.circle(path[i - 1].x + 0.5f, path[i-1].y + 0.5f, 0.5f);
        }
    }
}

