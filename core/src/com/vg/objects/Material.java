package com.vg.objects;

import com.badlogic.gdx.graphics.Texture;
import com.vg.ai.assets.MaterialAssets;

public class Material {
    public final int mass;
    public final Texture texture;
    public final int type;
    public Material(int type) {
        this.type = type;
        texture = MaterialAssets.materials[MaterialAssets.BRICK];
        mass = MaterialAssets.mass[MaterialAssets.BRICK];
    }
}
