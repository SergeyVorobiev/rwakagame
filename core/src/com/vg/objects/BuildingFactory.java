package com.vg.objects;

import com.vg.BuildingDrawer;
import com.vg.DrawSelector;
import com.vg.ai.assets.MaterialAssets;

public class BuildingFactory {
    private static DrawSelector<BuildingObject> selector = new BuildingDrawer();
    public static OneTileBuildingObject getWall() {
        int[] resources = new int[]{MaterialAssets.BRICK, 20};
        return new OneTileBuildingObject(selector,1000, 0, resources);
    }

    public static BuildingObject getFactory() {
        int[] resources = new int[]{MaterialAssets.BRICK, 200};
        return new BuildingObject(selector, 3, 2, 10000, 1, resources);
    }
}
