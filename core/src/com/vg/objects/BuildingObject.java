package com.vg.objects;

import com.vg.DrawSelector;
import com.vg.GameInstruments;
import com.vg.ai.search.TileInfoBuilder;

public class BuildingObject extends GameObject<BuildingObject> {
    public final static int CANNOT_BUILD = -3;
    public final static int CANNOT_BE_PLACED = -2;
    public final static int CAN_BE_PLACED = -1;
    public final static int LAYOUT = 0;
    public final static int LAYOUT_WITH_RESOURCES = 1;
    public final static int READY_TO_BUILD = 2;
    public final static int BUILDING = 3;
    public final static int BUILT = 4;
    public final int[] needResources;
    public float currentStrength;

    private int startTileXPos;
    private int startTileYPos;

    public BuildingObject (DrawSelector<BuildingObject> selector, int tileWidth, int tileHeight, float strength, int
            type, int[]
            needResources) {
        this(selector, tileWidth, tileHeight, strength, type, needResources, -1,  -1);
    }

    public BuildingObject(DrawSelector<BuildingObject> selector, int tileWidth, int tileHeight, float
            strength, int type, int[] needResources, int startTileXPos, int startTileYPos) {
        super(selector, tileWidth, tileHeight, strength, type);
        this.needResources = needResources;
        this.startTileXPos = startTileXPos;
        this.startTileYPos = startTileYPos;
        setTilePos(startTileXPos, startTileYPos);
        setState(CANNOT_BUILD);
    }

    public void setTilePos(int x, int y) {
        if (x == startTileXPos && y == startTileYPos)
            return;
        startTileXPos = x;
        startTileYPos = y;
        if (!GameInstruments.isInBoundsX(startTileXPos) || !GameInstruments.isInBoundsY(startTileYPos) ||
                !GameInstruments.isInBoundsX(startTileXPos + tileWidth - 1) ||
                !GameInstruments.isInBoundsY(startTileYPos + tileHeight - 1)) {
            setState(CANNOT_BUILD);
            return;
        }
        startTile = GameInstruments.getTiles()[startTileXPos][startTileYPos];
        TileInfoBuilder.getTilesArea(area, startTile, tileWidth, tileHeight);
        TileInfoBuilder.getPerimeter(aroundTiles, startTileXPos - 1, startTileYPos - 1, tileWidth + 2, tileHeight
                + 2);
        for(int i = 0; i < area.length; i++) {
            if(area[i].getK() < 0) {
                setState(CANNOT_BE_PLACED);
                return;
            }
        }
        setState(CAN_BE_PLACED);
    }

    public void place() {
        if(state != CAN_BE_PLACED)
            return;
        setState(LAYOUT);
        for(int i = 0; i < area.length; i++) {
            area[i].staticObject = this;
            area[i].setK(-1);
        }
    }

    // Warning! it does not check type of resource here and count of it. It assumes that type of resource and count are
    // correct - count > 0.
    public int addNeedResource(int count, int type) {
        int balance = 0;
        setState(LAYOUT_WITH_RESOURCES);
        for(int i = 0; i < needResources.length; i+=2) {
            if(needResources[i] == type) {
                needResources[i + 1] -= count;
                balance = needResources[i + 1];
            }
        }
        for(int i = 1; i < needResources.length; i+=2) {
            if(needResources[i] > 0) {
                setState(LAYOUT_WITH_RESOURCES);
                return balance;
            }
        }
        setState(READY_TO_BUILD);
        return balance;
    }

    public int getNeedResource(int type) {
        for (int i = 0; i < needResources.length; i+=2) {
            if (needResources[i] == type) {
                return needResources[i + 1];
            }
        }
        return 0;
    }

    public float getProgress() {
        return currentStrength / strength;
    }

    public boolean build(float dProgress) {
        currentStrength += dProgress;
        if(currentStrength >= strength) {
            currentStrength = strength;
            setState(BUILT);
            return true;
        }
        setState(BUILDING);
        return false;
    }
}
