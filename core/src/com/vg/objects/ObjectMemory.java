package com.vg.objects;

import com.vg.ai.search.TileInfo;

public class ObjectMemory {
    public int neededResourceType;
    public int countOfResourceToTake;
    public TileInfo[] placesToGo;
    public int currentPlaceIndex;
    public TileInfo curPlaceToGo;
}
