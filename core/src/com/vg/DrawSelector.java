package com.vg;

public interface DrawSelector<T> {
     Drawer<T> getCurrentDrawer(int state);
}
