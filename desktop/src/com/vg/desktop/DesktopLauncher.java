package com.vg.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.vg.L;
import com.vg.RPGGame;

public class DesktopLauncher {
    private interface MyFunction {
        void func();
    }
    private static MyFunction draw = null;
	public static void main (String[] arg) {
		//runConsoleDesktopTests();
		startGame();
        //test();
        //test();
        //L.pr(i);
	}
    private static int i = 0;
	private static void test() {
        for(;i < 5; ++i) {
            break;
        }
    }
    private static void draw1() {
	    L.pr("draw1");
    }
    private static void draw2() {
	    L.pr("draw2");
    }
	private static void startGame() {
        System.setProperty("user.name","Public");
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.width = 1200;
        config.height = 650;
        new LwjglApplication(new RPGGame(), config);
    }

}
